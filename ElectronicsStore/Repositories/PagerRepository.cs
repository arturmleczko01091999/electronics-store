﻿using System.Collections.Generic;
using System.Linq;
using ElectronicsStore.Data;
using ElectronicsStore.Models;

namespace ElectronicsStore.Repositories
{
    public interface IPagerRepository
    {
        ProductPager GetProductPager(List<Product> products, int page, int quantityPerPage = 6);
        NewsPager GetNewsPager(List<News> news, int page, int quantityPerPage = 3);
    }

    public class PagerRepository: IPagerRepository
    {
        private readonly ApplicationDbContext _db;

        public PagerRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public ProductPager GetProductPager(List<Product> products, int page, int quantityPerPage = 6)
        {
            ProductPager productPager = new ProductPager();
            int pageSize = quantityPerPage;

            if (page < 1)
            {
                page = 1;
            }

            int recsCount = products.Count();

            Pager pager = new Pager(recsCount, page, pageSize);

            int recSkip = (page - 1) * pageSize;

            List<Product> productsPagination = products
                .Skip(recSkip)
                .Take(pager.PageSize)
                .ToList();

            productPager.Pager = pager;
            productPager.Products = productsPagination;

            return productPager;
        }

        public NewsPager GetNewsPager(List<News> news, int page, int quantityPerPage = 3)
        {
            NewsPager newsPager = new NewsPager();
            int pageSize = quantityPerPage;

            if (page < 1)
            {
                page = 1;
            }

            int recsCount =  news.Count();

            Pager pager = new Pager(recsCount, page, pageSize);

            int recSkip = (page - 1) * pageSize;

            List<News> newsPagination = news
                .Skip(recSkip)
                .Take(pager.PageSize)
                .ToList();

            newsPager.Pager = pager;
            newsPager.News = newsPagination;

            return newsPager;
        }
    }
}
