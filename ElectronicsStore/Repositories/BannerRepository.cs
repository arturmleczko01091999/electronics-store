﻿using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using ElectronicsStore.Models;

namespace ElectronicsStore.Repositories
{
    public interface IBannerRepository
    {
        List<Banner> GetBanners();
    }

    public class BannerRepository: IBannerRepository
    {
        private readonly StreamReader _bannersJSON;

        public BannerRepository()
        {
            _bannersJSON = new StreamReader("wwwroot/data/banners.json");
        }

        public List<Banner> GetBanners()
        {
            string bannersJSONString = _bannersJSON.ReadToEnd();
            return JsonSerializer.Deserialize<List<Banner>>(bannersJSONString);
        }
    }
}
