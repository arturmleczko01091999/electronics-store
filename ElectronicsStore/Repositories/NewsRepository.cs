﻿using System.Collections.Generic;
using System.Linq;
using ElectronicsStore.Data;
using ElectronicsStore.Models;

namespace ElectronicsStore.Repositories
{
    public interface INewsRepository
    {
        void AddNews(News news);
        void UpdateNews(News news);
        void DeleteNews(News news);
        News GetNews(int id);
        News GetLastNews();
        List<News> GetAllNews();
        List<News> GetLastThreeNews();
        List<News> GetRecommendedNews();
    }

    public class NewsRepository: INewsRepository
    {
        private readonly ApplicationDbContext _db;

        public NewsRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public void AddNews(News news)
        {
            _db.News.Add(news);
            _db.SaveChanges();
        }

        public void UpdateNews(News news)
        {
            _db.News.Update(news);
            _db.SaveChanges();
        }

        public void DeleteNews(News news)
        {
            _db.News.Remove(news);
            _db.SaveChanges();
        }

        public News GetNews(int id)
        {
            return _db.News
                .Where(news => news.NewsId == id)
                .FirstOrDefault();
        }

        public News GetLastNews()
        {
            return _db.News
                .OrderBy(news => news.NewsId)
                .LastOrDefault();
        }

        public List<News> GetAllNews()
        {
            return _db.News
                .OrderByDescending(news => news.Date)
                .ToList();
        }

        public List<News> GetLastThreeNews()
        {
            return _db.News
                .OrderByDescending(news => news.Date)
                .Take(3)
                .ToList();
        }

        public List<News> GetRecommendedNews()
        {
            return _db.News
                .Where(news => news.isRecommended == true)
                .OrderByDescending(news => news.Date)
                .ToList();
        }
    }
}
