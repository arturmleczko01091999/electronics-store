﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ElectronicsStore.Data;
using ElectronicsStore.Models;

namespace ElectronicsStore.Repositories
{
    public interface ICategoryRepository
    {

        void AddCategory(Category category);
        void UpdateCategory(Category category);
        void DeleteCategory(Category category);
        Category GetCategory(int id);
        Category GetCurrentCategory(int? id);
        Category GetLastCategory();
        List<Category> GetAllCategories();
    }

    public class CategoryRepository: ICategoryRepository
    {
        private readonly ApplicationDbContext _db;

        public CategoryRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public void AddCategory(Category category)
        {
            _db.Categories.Add(category);
            _db.SaveChanges();
        }

        public void UpdateCategory(Category category)
        {
            _db.Categories.Update(category);
            _db.SaveChanges();
        }

        public void DeleteCategory(Category category)
        {
            _db.Categories.Remove(category);
            _db.SaveChanges();
        }

        public Category GetCategory(int id)
        {
            return _db.Categories
                .Where(category => category.CategoryId == id)
                .AsNoTracking()
                .FirstOrDefault();
        }

        public Category GetCurrentCategory(int? id)
        {
            return _db.Categories
                .Where(category => category.CategoryId == id)
                .Include(category => category.Products)
                .FirstOrDefault();
        }

        public Category GetLastCategory()
        {
            return _db.Categories
                .OrderBy(category => category.CategoryId)
                .LastOrDefault();
        }

        public List<Category> GetAllCategories()
        {
            return _db.Categories
                .OrderBy(category => category.CategoryId)
                .Include(category => category.Products)
                .ToList();
        }
    }
}
