﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ElectronicsStore.Data;
using ElectronicsStore.Models;

namespace ElectronicsStore.Repositories
{
    public interface IProductImageRepository
    {
        void AddProductImage(ProductImage productImage);
        void UpdateProductImage(ProductImage productImage);
        void DeleteProductImage(ProductImage productImage);
        ProductImage GetProductImage(int id);
        ProductImage GetLastProductImage();
        List<ProductImage> GetProductImages(int productId);
    }

    public class ProductImageRepository: IProductImageRepository
    {
        private readonly ApplicationDbContext _db;

        public ProductImageRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public void AddProductImage(ProductImage productImage)
        {
            _db.ProductImages.Add(productImage);
        }

        public void UpdateProductImage(ProductImage productImage)
        {
            _db.ProductImages.Update(productImage);
            _db.SaveChanges();
        }

        public void DeleteProductImage(ProductImage productImage)
        {
            _db.ProductImages.Remove(productImage);
            _db.SaveChanges();
        }

        public ProductImage GetProductImage(int id)
        {
            return _db.ProductImages
                .Where(productImage => productImage.ProductImageId == id)
                .AsNoTracking()
                .FirstOrDefault();
        }

        public ProductImage GetLastProductImage()
        {
            return _db.ProductImages
                .OrderBy(productImage => productImage.ProductImageId)
                .LastOrDefault();
        }

        public List<ProductImage> GetProductImages(int productId)
        {
            return _db.ProductImages
                .Where(productImage => productImage.ProductId == productId)
                .ToList();
        }
    }
}
