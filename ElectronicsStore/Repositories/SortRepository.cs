﻿using System.Collections.Generic;
using System.Linq;
using ElectronicsStore.Models;

namespace ElectronicsStore.Repositories
{
    public interface ISortRepository
    {
        SortProductsOption GetCurrentSortProductOption(List<SortProductsOption> sortProductsOptions, string sortOrder);
        List<Product> GetSortedProducts(List<Product> products, string sortOrder);
    }

    public class SortRepository: ISortRepository
    {
        public SortProductsOption GetCurrentSortProductOption(List<SortProductsOption> sortProductsOptions, string sortOrder)
        {
            SortProductsOption currentSortingOption;

            if (sortOrder != null)
            {
                currentSortingOption = sortProductsOptions
                    .Where(sortProductOption => sortProductOption.SortType == sortOrder)
                    .FirstOrDefault();
            }
            else
            {
                currentSortingOption = sortProductsOptions
                    .First();
            }

            return currentSortingOption;
        }

        public List<Product> GetSortedProducts(List<Product> products, string sortOrder)
        {
            List<Product> sortedProducts;

            switch (sortOrder)
            {

                case "priceAsc":
                    sortedProducts = products
                        .OrderBy(product => product.Price)
                        .ToList();
                    break;
                case "priceDesc":
                    sortedProducts = products
                        .OrderByDescending(product => product.Price)
                        .ToList();
                    break;
                case "dateAsc":
                    sortedProducts = products
                        .OrderBy(product => product.ProductId)
                        .ToList();
                    break;
                case "dateDesc":
                    sortedProducts = products
                        .OrderByDescending(product => product.ProductId)
                        .ToList();
                    break;
                default:
                    sortedProducts = products
                        .OrderBy(product => product.Name)
                        .ToList();
                    break;
            }

            return sortedProducts;
        }
    }
}
