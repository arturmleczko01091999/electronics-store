﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ElectronicsStore.Data;
using ElectronicsStore.Enums;
using ElectronicsStore.Models;

namespace ElectronicsStore.Repositories
{
    public interface IProductRepository
    {
        void AddProduct(Product product);
        void UpdateProduct(Product product);
        void DeleteProduct(Product product);
        Product GetProduct(int id);
        Product GetLatestProduct();
        List<Product> GetProducts();
        List<Product> GetMostPopularProducts();
        List<Product> GetOutletProducts();
        List<Product> GetProductsOnPromotion();
        List<Product> GetNewProducts();
        List<Product> GetProductsFromCategory(int? id);
        List<Product> GetSimilarProducts(Product currentProduct);
        List<Product> GetLatestProducts(int quantity);
    }

    public class ProductRepository : IProductRepository
    {
        private readonly ApplicationDbContext _db;

        public ProductRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public void AddProduct(Product product)
        {
            _db.Products.Add(product);
            _db.SaveChanges();
        }

        public void UpdateProduct(Product product)
        {
            _db.Products.Update(product);
            _db.SaveChanges();
        }

        public void DeleteProduct(Product product)
        {
            _db.Products.Remove(product);
            _db.SaveChanges();
        }

        public Product GetProduct(int id)
        {
            return _db.Products
                .Where(product => product.ProductId == id)
                .Include(product => product.Images)
                .FirstOrDefault();
        }

        public Product GetLatestProduct()
        {
            return _db.Products
                .OrderBy(product => product.ProductId)
                .LastOrDefault();
        }

        public List<Product> GetProducts()
        {
            return _db.Products
                .Include(product => product.Category)
                .ToList();
        }

        public List<Product> GetMostPopularProducts()
        {
            return _db.Products
                .Where(product => product.IsMostPopular == true)
                .Include(product => product.Images)
                .ToList();
        }

        public List<Product> GetOutletProducts()
        {
            return _db.Products
                .Where(product => product.SpecialOffer == SpecialOffer.Outlet)
                .Include(product => product.Images)
                .ToList();
        }

        public List<Product> GetProductsOnPromotion()
        {
            return _db.Products
                .Where(product => product.SpecialOffer == SpecialOffer.Promotion)
                .ToList();
        }

        public List<Product> GetNewProducts()
        {
            return _db.Products
                .Where(product => product.SpecialOffer == SpecialOffer.New)
                .ToList();
        }

        public List<Product> GetProductsFromCategory(int? id)
        {
            return _db.Products
                .Where(product => product.CategoryId == id)
                .Include(product => product.Images)
                .ToList();
        }

        public List<Product> GetSimilarProducts(Product currentProduct)
        {
            return _db.Products
                .Where(product => product.CategoryId == currentProduct.CategoryId && product.ProductId != currentProduct.ProductId)
                .Include(product => product.Images)
                .ToList();
        }

        public List<Product> GetLatestProducts(int quantity)
        {
            return _db.Products
                .OrderByDescending(product => product.ProductId)
                .Take(quantity)
                .ToList();
        }
    }
}
