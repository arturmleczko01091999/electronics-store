using System.Collections.Generic;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using ElectronicsStore.Models;
using ElectronicsStore.Repositories;

namespace ElectronicsStore.Controllers
{
    public class ProductCardController : Controller
    {
        private ICategoryRepository _categoryRepository;
        private IProductRepository _productRepository;

        public ProductCardController(ICategoryRepository categoryRepository, IProductRepository productRepository)
        {
            _categoryRepository = categoryRepository;
            _productRepository = productRepository;
        }

        public IActionResult Index(int id)
        {
            ProductCardView productCardView = new ProductCardView();

            Product product = _productRepository.GetProduct(id);
            Category currentCategory = _categoryRepository.GetCurrentCategory(product.CategoryId);
            List<ProductInformation> informations = JsonSerializer.Deserialize<List<ProductInformation>>(product.Information);
            List<ProductInformation> characteristics = product.Characteristics != null ? JsonSerializer.Deserialize<List<ProductInformation>>(product.Characteristics) : null;
            List<Product> similarProducts = _productRepository.GetSimilarProducts(product);


            productCardView.Product = product;
            productCardView.CurrentCategory = currentCategory;
            productCardView.Informations = informations;
            productCardView.Characteristics = characteristics;
            productCardView.SimilarProducts = similarProducts;

            return View(productCardView);
        }
    }
}