using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ElectronicsStore.Models;

namespace ElectronicsStore.Controllers
{
    [Authorize(Roles = "Klient")]
    public class AccountSettingsController : Controller
    {
        private UserManager<User> _userManager;

        public AccountSettingsController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            User user = _userManager.GetUserAsync(User).Result;
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(User model)
        {
            User user = _userManager.GetUserAsync(User).Result;

            if (ModelState.IsValid)
            {
                user.UserName = model.Email;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.Street = model.Street;
                user.StreetNumber = model.StreetNumber;
                user.NumberOfPremises = model.NumberOfPremises;
                user.ZipCode = model.ZipCode;
                user.City = model.City;
                user.Country = model.Country;
                user.Email = model.Email;
                user.Phone = model.Phone;

                await _userManager.UpdateAsync(user);
            }

            return View(model);
        }
    }
}