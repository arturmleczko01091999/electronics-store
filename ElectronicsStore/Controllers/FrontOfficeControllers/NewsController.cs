﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ElectronicsStore.Models;
using ElectronicsStore.Repositories;

namespace ElectronicsStore.Controllers
{
    public class NewsController : Controller
    {
        private INewsRepository _newsRepository;
        private IPagerRepository _pagerRepository;

        public NewsController(INewsRepository newsRepository, IPagerRepository pagerRepository)
        {
            _newsRepository = newsRepository;
            _pagerRepository = pagerRepository;
        }

        public IActionResult Index(int page = 1)
        {
            NewsView newsView = new NewsView();

            List<News> allArticles = _newsRepository.GetAllNews();
            List<News> recommendedArticles = _newsRepository.GetRecommendedNews();
            NewsPager newsPager = _pagerRepository.GetNewsPager(allArticles, page);

            newsView.NewsPager = newsPager;
            newsView.recommendedArticles = recommendedArticles;

            return View(newsView);
        }

        public IActionResult Article(int id)
        {
            News article = _newsRepository.GetNews(id);

            return View(article);
        }
    }
}
