﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using ElectronicsStore.Models;
using ElectronicsStore.Repositories;

namespace ElectronicsStore.Controllers
{
    public class HomeController : Controller
    {
        private IProductRepository _productRepository;
        private INewsRepository _newsRepository;
        private IBannerRepository _bannerRepository;

        public HomeController(IProductRepository productRepository, INewsRepository newsRepository)
        {
            _productRepository = productRepository;
            _newsRepository = newsRepository;

            _bannerRepository = new BannerRepository();
        }

        public IActionResult Index()
        {
            HomeView model = new HomeView();

            model.Banners = _bannerRepository.GetBanners();
            model.MostPopularProducts = _productRepository.GetMostPopularProducts();
            model.Outlets = _productRepository.GetOutletProducts();
            model.News = _newsRepository.GetLastThreeNews();

            return View(model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
