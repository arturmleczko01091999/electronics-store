using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ElectronicsStore.Models;
using ElectronicsStore.Repositories;

namespace ElectronicsStore.Controllers
{
    public class ProductsController : Controller
    {
        private ICategoryRepository _categoryRepository;
        private IProductRepository _productRepository;
        private IPagerRepository _pagerRepository;
        private ISortRepository _sortRepository;

        public ProductsController(
            ICategoryRepository categoryRepository,
            IProductRepository productRepository,
            IPagerRepository pagerRepository,
            ISortRepository sortRepository)
        {
            _categoryRepository = categoryRepository;
            _productRepository = productRepository;
            _pagerRepository = pagerRepository;
            _sortRepository = sortRepository;
        }

        public IActionResult Category(int? id, string sortOrder, int page = 1)
        {
            ProductsView productsView = new ProductsView();
            SortProducts sortProducts = new SortProducts();

            productsView.Categories = _categoryRepository.GetAllCategories();

            sortProducts.SortProductsOptions = new List<SortProductsOption>()
            {
                new SortProductsOption { SortType = "", SortName = "Sortowanie domyślne"},
                new SortProductsOption { SortType = "priceAsc", SortName = "Cena: od najtańszych"},
                new SortProductsOption { SortType = "priceDesc", SortName = "Cena: od najdroższych"},
                new SortProductsOption { SortType = "dateAsc", SortName = "Data dodania: najstarsza"},
                new SortProductsOption { SortType = "dateDesc", SortName = "Data dodania: najnowsza"},
            };

            if (id != null)
            {
                productsView.CurrentCategory = _categoryRepository.GetCurrentCategory(id);
                productsView.ProductsFromCurrentCategory = _productRepository.GetProductsFromCategory(id);

                List<Product> productsFromCurrentCategory = _sortRepository.GetSortedProducts(productsView.ProductsFromCurrentCategory, sortOrder);
                sortProducts.CurrentSortingOption = _sortRepository.GetCurrentSortProductOption(sortProducts.SortProductsOptions, sortOrder);

                productsView.SortProducts = sortProducts;
                productsView.ProductPager = _pagerRepository.GetProductPager(productsFromCurrentCategory, page);

                ViewBag.CurrentSortingOption = sortProducts.CurrentSortingOption.SortType;
                ViewBag.CurrentPage = productsView.ProductPager.Pager.CurrentPage;
            }

            return View(productsView);
        }
    }
}