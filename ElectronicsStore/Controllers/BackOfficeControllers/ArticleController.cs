﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ElectronicsStore.Models;
using ElectronicsStore.Repositories;

namespace ElectronicsStore.Controllers
{
    public class ArticleController : Controller
    {
        private INewsRepository _newsRepository;
        private IPagerRepository _pagerRepository;

        public ArticleController(INewsRepository newsRepository, IPagerRepository pagerRepository)
        {
            _newsRepository = newsRepository;
            _pagerRepository = pagerRepository;
        }

        public IActionResult Articles(int page = 1)
        {
            NewsPager newsPager = new NewsPager();
            List<News> articles = _newsRepository.GetAllNews();

            newsPager = _pagerRepository.GetNewsPager(articles, page, 10);

            return View(newsPager);
        }

        public IActionResult AddArticle()
        {
            return View();
        }

        public IActionResult EditArticle(int id)
        {
            News article = _newsRepository.GetNews(id);
            return View(article);
        }
    }
}
