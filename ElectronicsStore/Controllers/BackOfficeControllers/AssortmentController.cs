﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ElectronicsStore.Data;
using ElectronicsStore.Models;
using ElectronicsStore.Repositories;

namespace ElectronicsStore.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AssortmentController : Controller
    {
        private ApplicationDbContext _db;
        private IWebHostEnvironment _hostingEnvironment;
        private IProductRepository _productRepository;
        private IProductImageRepository _productImageRepository;
        private IPagerRepository _pagerRepository;
        private ICategoryRepository _categoryRepository;
        private ISortRepository _sortRepository;

        public AssortmentController(
            ApplicationDbContext db,
            IProductRepository productRepository,
            IProductImageRepository productImageRepository,
            IPagerRepository pagerRepository,
            ICategoryRepository categoryRepository,
            ISortRepository sortRepository,
            IWebHostEnvironment environment)
        {
            _db = db;
            _hostingEnvironment = environment;

            _productRepository = productRepository;
            _productImageRepository = productImageRepository;
            _pagerRepository = pagerRepository;
            _categoryRepository = categoryRepository;
            _sortRepository = sortRepository;
        }

        [HttpGet]
        public IActionResult Products(int page = 1)
        {
            AssortmentProductsView assortmentProductsView = new AssortmentProductsView();

            List<Product> products = _productRepository.GetProducts();
            List<Product> productsDataDesc = _sortRepository.GetSortedProducts(products, "dateDesc");

            assortmentProductsView.ProductPager = _pagerRepository.GetProductPager(productsDataDesc, page, 10);
            assortmentProductsView.LatestProducts = _productRepository.GetLatestProducts(3);
            assortmentProductsView.ProductsOnPromotion = _productRepository.GetProductsOnPromotion();
            assortmentProductsView.NewProducts = _productRepository.GetNewProducts();
            assortmentProductsView.OutletProducts = _productRepository.GetOutletProducts();

            return View(assortmentProductsView);
        }

        [HttpGet]
        public IActionResult AddProduct()
        {
            SetCategoriesInGlobalVariable();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddProduct(ProductView model)
        {
            if (ModelState.IsValid)
            {
                model.Product.ProductId = _productRepository.GetLatestProduct() != null
                    ? _productRepository.GetLatestProduct().ProductId + 1
                    : 1;

                if (model.ProductImageFile != null)
                {
                    AddPhoto(model.Product.ProductId, model.ProductImage, model.ProductImageFile);
                }

                _productRepository.AddProduct(model.Product);

                return RedirectToAction("EditProduct", new { id = model.Product.ProductId });
            }

            SetCategoriesInGlobalVariable();

            return View(model);
        }

        [HttpGet]
        public IActionResult EditProduct(int id)
        {
            ProductView productViewModel = new ProductView();
            productViewModel.Product = _productRepository.GetProduct(id);

            SetCategoriesInGlobalVariable();

            return View(productViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditProduct(ProductView model)
        {
            if (ModelState.IsValid)
            {
                Product product = model.Product;

                if (model.ProductImageFile != null)
                {
                    AddPhoto(model.Product.ProductId, model.ProductImage, model.ProductImageFile);
                    _db.SaveChanges();

                    model.ProductImageFile = null;
                    model.ProductImage = null;
                }

                model.Product.Images = _productImageRepository.GetProductImages(product.ProductId);

                _productRepository.UpdateProduct(product);
            }

            SetCategoriesInGlobalVariable();
            return RedirectToAction("EditProduct", new { id = model.Product.ProductId });
        }

        [HttpGet]
        public IActionResult Categories()
        {
            List<Category> categories = _categoryRepository.GetAllCategories();

            return View(categories);
        }

        
        private void AddPhoto(int productId, ProductImage image, IFormFile imageFile)
        {
            string uniqueFileName = GetUniqueFileName(imageFile.FileName);
            string imageDirectory = Path.Combine(_hostingEnvironment.WebRootPath, "images");
            string filePath = Path.Combine(imageDirectory, uniqueFileName);

            ProductImage productImage = new ProductImage();

            int productImageId = _productImageRepository.GetLastProductImage() != null
                ? _productImageRepository.GetLastProductImage().ProductImageId + 1
                : 1;

            productImage.ProductImageId = productImageId;
            productImage.Title = image.Title;
            productImage.Url = $"/images/{uniqueFileName}";
            productImage.IsMain = image.IsMain;
            productImage.ProductId = productId;

            _productImageRepository.AddProductImage(productImage);

            imageFile.CopyTo(new FileStream(filePath, FileMode.Create));
        }

        private string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName);
        }

        private void SetCategoriesInGlobalVariable()
        {
            ViewData["categories"] = _categoryRepository.GetAllCategories();
        }
    }
}
