﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ElectronicsStore.Models;
using ElectronicsStore.Repositories;
using ElectronicsStore.Utility;

namespace ElectronicsStore.Controllers
{
    [Route("api/articles")]
    public class RESTArticleController : Controller
    {
        private INewsRepository _newsRepository;
        private IWebHostEnvironment _hostingEnvironment;

        public RESTArticleController(INewsRepository newsRepository, IWebHostEnvironment hostingEnvironment)
        {
            _newsRepository = newsRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        public IActionResult AddArticle([FromBody] News article)
        {
            article.NewsId = _newsRepository.GetLastNews() != null
                ? _newsRepository.GetLastNews().NewsId + 1
                : 1;

            _newsRepository.AddNews(article);

            return Created($"api/articles/{article.NewsId}", article);
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult UpdateArticle(int id, [FromBody] News article)
        {
            News articleToUpdate = _newsRepository.GetNews(id);

            if (articleToUpdate == null)
            {
                return NotFound();
            }

            if (articleToUpdate.ImageUrl != article.ImageUrl)
            {
                LocalImageHelper.deleteLocalPhoto(_hostingEnvironment, articleToUpdate.ImageUrl);
            }

            articleToUpdate.Title = article.Title;
            articleToUpdate.ImageTitle = article.ImageTitle;
            articleToUpdate.ImageUrl = article.ImageUrl;
            articleToUpdate.Content = article.Content;
            articleToUpdate.Date = article.Date;
            articleToUpdate.isRecommended = article.isRecommended;

            _newsRepository.UpdateNews(articleToUpdate);

            return Ok(articleToUpdate);
        }

        [HttpPatch]
        [Route("{id}")]
        public IActionResult UpdateArticleContent(int id, [FromBody] News article)
        {
            News articleToUpdate = _newsRepository.GetNews(id);

            if (articleToUpdate == null)
            {
                return NotFound();
            }

            if (article.Title != null)
            {
                articleToUpdate.Title = article.Title;
            }

            if (article.ImageTitle != null)
            {
                articleToUpdate.ImageTitle = article.ImageTitle;
            }

            if (article.ImageUrl != null)
            {
                articleToUpdate.ImageUrl = article.ImageUrl;
            }

            if (article.Content != null)
            {
                articleToUpdate.Content = article.Content;
            }

            if (article.isRecommended != articleToUpdate.isRecommended)
            {
                articleToUpdate.isRecommended = article.isRecommended;
            }

            _newsRepository.UpdateNews(articleToUpdate);

            return Ok(articleToUpdate);
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteArticle(int id)
        {
            News newsToDelete = _newsRepository.GetNews(id);

            if (newsToDelete == null)
            {
                return NotFound();
            }

            LocalImageHelper.deleteLocalPhoto(_hostingEnvironment, newsToDelete.ImageUrl);
            _newsRepository.DeleteNews(newsToDelete);

            return Ok();
        }
    }
}

