﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ElectronicsStore.Models;
using ElectronicsStore.Repositories;
using ElectronicsStore.Utility;

namespace ElectronicsStore.Controllers
{
    [Route("api/products/photos")]
    public class RESTProductPhotoController: Controller
    {
        private IWebHostEnvironment _hostingEnvironment;
        private IProductImageRepository _productImageRepository;

        public RESTProductPhotoController(IProductImageRepository productImageRepository, IWebHostEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _productImageRepository = productImageRepository;
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult UpdateProductPhoto(int id, [FromBody] ProductImage productImage)
        {
            ProductImage productImageToUpdate = _productImageRepository.GetProductImage(id);

            if (productImageToUpdate == null)
            {
                return NotFound();
            }

            if (productImageToUpdate.Url != productImage.Url)
            {
                LocalImageHelper.deleteLocalPhoto(_hostingEnvironment, productImageToUpdate.Url);
            }

            productImageToUpdate.Title = productImage.Title;
            productImageToUpdate.Url = productImage.Url;
            productImageToUpdate.IsMain = productImage.IsMain;

            _productImageRepository.UpdateProductImage(productImageToUpdate);

            return Ok(productImageToUpdate);
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteProductPhoto(int id)
        {
            ProductImage productPhotoToDelete = _productImageRepository.GetProductImage(id);

            if (productPhotoToDelete == null)
            {
                return NotFound();
            }

            LocalImageHelper.deleteLocalPhoto(_hostingEnvironment, productPhotoToDelete.Url);

            _productImageRepository.DeleteProductImage(productPhotoToDelete);

            return Ok(productPhotoToDelete);
        }
    }
}
