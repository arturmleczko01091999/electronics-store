﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using File = ElectronicsStore.Models.File;

namespace ElectronicsStore.Controllers
{
    [Route("api/files")]
    public class RESTFileController : Controller
    {
        private IWebHostEnvironment _hostingEnvironment;

        public RESTFileController(IWebHostEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        [Route("photos")]
        public IActionResult AddPhotoFile([FromForm] File photoFile)
        {
            string imageDirectory = Path.Combine(_hostingEnvironment.WebRootPath, "images");
            string filePath = Path.Combine(imageDirectory, photoFile.FileName);
            photoFile.FormFile.CopyTo(new FileStream(filePath, FileMode.Create));

            return Created($"api/files/photos", photoFile);
        }
    }
}
