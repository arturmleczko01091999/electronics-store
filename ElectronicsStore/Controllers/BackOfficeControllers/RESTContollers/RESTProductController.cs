﻿using Microsoft.AspNetCore.Mvc;
using ElectronicsStore.Models;
using ElectronicsStore.Repositories;
using System.Linq;
using ElectronicsStore.Utility;
using Microsoft.AspNetCore.Hosting;

namespace ElectronicsStore.Controllers
{
    [Route("api/products")]
    public class RESTProductController: Controller
    {
        private IWebHostEnvironment _hostingEnvironment;
        private IProductRepository _productRepository;

        public RESTProductController(IProductRepository productRepository, IWebHostEnvironment hostingEnvironment)
        {
            _productRepository = productRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteProduct(int id)
        {
            Product productToDelete = _productRepository.GetProduct(id);

            if (productToDelete == null)
            {
                return NotFound();
            }

            _productRepository.DeleteProduct(productToDelete);

            if (productToDelete.Images.Count() > 0)
            {
                foreach(ProductImage productImage in productToDelete.Images)
                {
                    LocalImageHelper.deleteLocalPhoto(_hostingEnvironment, productImage.Url);
                }
            }

            return Ok(productToDelete);
        }
    }
}
