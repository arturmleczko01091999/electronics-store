﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ElectronicsStore.Models;
using ElectronicsStore.Repositories;
using ElectronicsStore.Utility;

namespace ElectronicsStore.Controllers
{
    [Route("api/categories")]
    public class RESTCategoryController : Controller
    {
        private IWebHostEnvironment _hostingEnvironment;
        private ICategoryRepository _categoryRepository;

        public RESTCategoryController(ICategoryRepository categoryRepository, IWebHostEnvironment hostingEnvironment)
        {
            _categoryRepository = categoryRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        public IActionResult AddCategory([FromBody] Category category)
        {
            category.CategoryId = _categoryRepository.GetLastCategory() != null
                ? _categoryRepository.GetLastCategory().CategoryId + 1
                : 1;

            _categoryRepository.AddCategory(category);

            return Created($"api/categories/{category.CategoryId}", category);
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult UpdateCategory(int id, [FromBody] Category category)
        {
            Category categoryToUpdate = _categoryRepository.GetCategory(id);

            if (categoryToUpdate == null)
            {
                return NotFound();
            }

            if (categoryToUpdate.ImageUrl != category.ImageUrl)
            {
                LocalImageHelper.deleteLocalPhoto(_hostingEnvironment, categoryToUpdate.ImageUrl);
            }

            categoryToUpdate.Name = category.Name;
            categoryToUpdate.ImageTitle = category.ImageTitle;
            categoryToUpdate.ImageUrl = category.ImageUrl;

            _categoryRepository.UpdateCategory(categoryToUpdate);

            return Ok(categoryToUpdate);
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteCategory(int id)
        {
            Category categoryToDelete = _categoryRepository.GetCategory(id);

            if (categoryToDelete == null)
            {
                return NotFound();
            }

            LocalImageHelper.deleteLocalPhoto(_hostingEnvironment, categoryToDelete.ImageUrl);

            _categoryRepository.DeleteCategory(categoryToDelete);

            return Ok();
        }


        [HttpGet]
        [Route("products/chart")]
        public IActionResult GetCategoryProductsChart()
        {
            List<string> labels = new List<string>();
            List<int> data = new List<int>();

            List<Category> categories = _categoryRepository.GetAllCategories();

            if (categories == null)
            {
                return NotFound();
            }

            foreach (Category category in categories)
            {
                if (category.Name == null || category.Products == null)
                {
                    return NotFound();
                }
                else
                {
                    labels.Add(category.Name);
                    data.Add(category.Products.Count());
                }
            }

            Chart chart = new Chart();

            chart.labels = labels.ToArray();
            chart.data = data.ToArray();

            return new OkObjectResult(chart);
        }
    }
}
