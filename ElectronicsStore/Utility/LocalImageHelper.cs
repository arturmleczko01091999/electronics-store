﻿using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace ElectronicsStore.Utility
{
    public static class LocalImageHelper
    {
        public static void deleteLocalPhoto(IWebHostEnvironment hostingEnvironmentstring, string url)
        {
            string imageToDeleteUrl = url.Substring(1);
            string pathToImageToDelete = Path.Combine(hostingEnvironmentstring.WebRootPath, imageToDeleteUrl);

            if (File.Exists(pathToImageToDelete))
            {
                File.Delete(pathToImageToDelete);
            }
        }
    }
}
