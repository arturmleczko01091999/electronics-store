﻿using System.Collections.Generic;
using ElectronicsStore.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ElectronicsStore.Utility
{
    public static class CategoriesHelper
    {
        public static List<SelectListItem> GetCountriesSelectList(List<Category> categories)
        {
            List<SelectListItem> categoriesSelectList = new List<SelectListItem>();

            foreach (Category category in categories)
            {
                categoriesSelectList.Add(new SelectListItem { Value = category.CategoryId.ToString(), Text = category.Name });
            }

            return categoriesSelectList;
        }
    }
}
