﻿using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using ElectronicsStore.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ElectronicsStore.Utility
{
    public static class CountryHelper
    {
        private static List<Country> GetCountries()
        {
            StreamReader countriesJSON = new StreamReader("wwwroot/data/countries.json");
            string countriesJSONString = countriesJSON.ReadToEnd();

            List<Country> countries = JsonSerializer.Deserialize<List<Country>>(countriesJSONString);

            return countries;
        }

        public static List<SelectListItem> GetCountriesSelectList()
        {
            List<Country> countries = GetCountries();

            List<SelectListItem> countriesSelectList = new List<SelectListItem>();

            foreach (Country country in countries)
            {
                if (country.code == "PL")
                {
                    countriesSelectList.Add(new SelectListItem { Value = country.name_pl, Text = country.name_pl, Selected = true });
                } else
                {
                    countriesSelectList.Add(new SelectListItem { Value = country.name_pl, Text = country.name_pl });
                }
            }

            return countriesSelectList;
        }
    }
}
