﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ElectronicsStore.Utility
{
    public static class RoleHelper
    {
        public static string Admin = "Administrator";
        public static string Client = "Klient";

        public static List<SelectListItem> GetRolesSelectList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = RoleHelper.Admin, Text = RoleHelper.Admin},
                new SelectListItem { Value = RoleHelper.Client, Text = RoleHelper.Client, Selected = true}
            };
        }
    }
}
