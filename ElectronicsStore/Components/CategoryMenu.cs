﻿using System;
using System.Collections.Generic;
using System.Linq;
using ElectronicsStore.Data;
using ElectronicsStore.Models;
using ElectronicsStore.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace ElectronicsStore.Components
{
    public class CategoryMenu: ViewComponent
    {
        private ICategoryRepository _categoryRepository;

        public CategoryMenu(ApplicationDbContext db)
        {
            _categoryRepository = new CategoryRepository(db);
        }

        public IViewComponentResult Invoke()
        {
            List<Category> categories = _categoryRepository.GetAllCategories();

            return View(categories);
        }
    }
}
