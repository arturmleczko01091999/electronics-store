﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using ElectronicsStore.Data;
using ElectronicsStore.Models;
using Microsoft.AspNetCore.Mvc;

namespace ElectronicsStore.Components
{
    public class Affiliates: ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            StreamReader afiliatesJSON = new StreamReader("wwwroot/data/afiliates.json");
            string afiliatesJSONString = afiliatesJSON.ReadToEnd();

            List<Affiliate> affiliates = JsonSerializer.Deserialize<List<Affiliate>>(afiliatesJSONString);

            return View(affiliates);
        }
    }
}
