﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ElectronicsStore.CutsomDataAnnotations
{
    public class BooleanMustBeTrueAttribute : ValidationAttribute
    {
        public override bool IsValid(object propertyValue)
        {
            return propertyValue != null
                && propertyValue is bool
                && (bool)propertyValue;
        }
    }
}
