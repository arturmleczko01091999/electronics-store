﻿using Microsoft.AspNetCore.Http;

namespace ElectronicsStore.Models
{
    public class File
    {
        public string FileName { get; set; }
        public IFormFile FormFile { get; set; }
    }
}
