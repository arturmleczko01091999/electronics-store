﻿namespace ElectronicsStore.Models
{
    public class ProductInformation
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
