﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace ElectronicsStore.Models
{
    public class User: IdentityUser 
    {
        [Required(ErrorMessage = "Imię jest wymagane")]
        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Nazwisko jest wymagane")]
        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Ulica jest wymagana")]
        [StringLength(58, ErrorMessage = "Podana nazwa ulicy jest zbyt długa")]
        [Display(Name = "Ulica")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Numer budynku jest wymagany")]
        [Display(Name = "Numer budynku")]
        public string StreetNumber { get; set; }

        [Display(Name = "Numer lokalu")]
        public string NumberOfPremises { get; set; }

        [Required(ErrorMessage = "Kod pocztowy jest wymagany")]
        [RegularExpression(@"^\d{2}(-\d{3})?$", ErrorMessage = "Podany kod pocztowy jest niepoprawny")]
        [Display(Name = "Kod pocztowy")]
        public string ZipCode { get; set; }

        [Required(ErrorMessage = "Miasto jest wymagane")]
        [StringLength(119, ErrorMessage = "Podana azwa miasta jest zbyt długa")]
        [Display(Name = "Miasto")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Kraj")]
        public string Country { get; set; }

        [Required(ErrorMessage = "Numer telefonu jest wymagany")]
        [Phone(ErrorMessage = "Podany numer telefonu jest niepoprawny")]
        [Display(Name = "Telefon")]
        public string Phone { get; set; }
    }
}
