﻿using System.Collections.Generic;

namespace ElectronicsStore.Models
{
    public class ProductsView
    {
        public List<Category> Categories { get; set; }
        public Category CurrentCategory { get; set; }
        public List<Product> ProductsFromCurrentCategory { get; set; }
        public SortProducts SortProducts { get; set; }
        public ProductPager ProductPager { get; set; }
    }
}
