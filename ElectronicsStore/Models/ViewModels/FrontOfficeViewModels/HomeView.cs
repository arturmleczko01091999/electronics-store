﻿using System.Collections.Generic;

namespace ElectronicsStore.Models
{
    public class HomeView
    {
        public List<Banner> Banners { get; set; }
        public List<Product> MostPopularProducts { get; set; }
        public List<Product> Outlets { get; set; }
        public List<News> News { get; set; }
    }
}
