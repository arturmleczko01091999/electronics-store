﻿using System.Collections.Generic;

namespace ElectronicsStore.Models
{
    public class ProductCardView
    {
        public Category CurrentCategory { get; set; }
        public Product Product { get; set; }
        public List<ProductInformation> Informations { get; set; }
        public List<ProductInformation> Characteristics { get; set; }
        public List<Product> SimilarProducts { get; set; }
    }
}
