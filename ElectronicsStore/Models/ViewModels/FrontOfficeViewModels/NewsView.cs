﻿using System.Collections.Generic;

namespace ElectronicsStore.Models
{
    public class NewsView
    {
        public List<News> recommendedArticles { get; set; }
        public NewsPager NewsPager { get; set; }
    }
}
