﻿using System;
using System.ComponentModel.DataAnnotations;
using ElectronicsStore.CutsomDataAnnotations;

namespace ElectronicsStore.Models
{
    public class RegisterView
    {
        [Required]
        [Display(Name = "Rola konta")]
        public string RoleName { get; set; }

        [Required(ErrorMessage = "Imię jest wymagane")]
        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Nazwisko jest wymagane")]
        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Ulica jest wymagana")]
        [StringLength(58, ErrorMessage = "Podana nazwa ulicy jest zbyt długa")]
        [Display(Name = "Ulica")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Numer budynku jest wymagany")]
        [Display(Name = "Numer budynku")]
        public string StreetNumber { get; set; }

        [Display(Name = "Numer lokalu")]
        public string NumberOfPremises { get; set; }

        [Required(ErrorMessage = "Kod pocztowy jest wymagany")]
        [RegularExpression(@"^\d{2}(-\d{3})?$", ErrorMessage = "Podany kod pocztowy jest niepoprawny")]
        [Display(Name = "Kod pocztowy")]
        public string ZipCode { get; set; }

        [Required(ErrorMessage = "Miasto jest wymagane")]
        [StringLength(119, ErrorMessage = "Podana azwa miasta jest zbyt długa")]
        [Display(Name = "Miasto")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Kraj")]
        public string Country { get; set; }

        [Required(ErrorMessage = "Numer telefonu jest wymagany")]
        [Phone(ErrorMessage = "Podany numer telefonu jest niepoprawny")]
        [Display(Name = "Telefon")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Email jest wymagany")]
        [EmailAddress(ErrorMessage = "Podany email jest niepoprawny")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Hasło jest wymagane")]
        [DataType(DataType.Password)]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{9,10}$", ErrorMessage = "Hasło musi zawierać od 8 do 10 znaków, mała literę, dużą literę, liczbę i znak spejclany")]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Ponowne podanie hasła jest wymagane")]
        [Compare("Password", ErrorMessage = "Podane muszą być identyczne")]
        [DataType(DataType.Password)]
        [Display(Name = "Powtórz hasło")]
        public string ConfirmPassword { get; set; }

        [BooleanMustBeTrue(ErrorMessage = "Aby się zarejestrować musisz zaakceptować regulamin sklepu")]
        public bool AcceptTermsAndConditions { get; set; }
    }
}
