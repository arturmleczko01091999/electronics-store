﻿using System.Collections.Generic;

namespace ElectronicsStore.Models
{
    public class AssortmentProductsView
    {
        public List<Product> Products { get; set; }
        public List<Product> LatestProducts { get; set; }
        public List<Product> ProductsOnPromotion { get; set; }
        public List<Product> NewProducts { get; set; }
        public List<Product> OutletProducts { get; set; }
        public ProductPager ProductPager { get; set; }
    }
}
