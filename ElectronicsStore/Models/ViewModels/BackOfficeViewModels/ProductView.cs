﻿using Microsoft.AspNetCore.Http;

namespace ElectronicsStore.Models
{
    public class ProductView
    {
        public Product Product { get; set; }
        public ProductImage ProductImage { get; set; }
        public IFormFile ProductImageFile { get; set; }
    }
}
