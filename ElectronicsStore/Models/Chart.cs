﻿namespace ElectronicsStore.Models
{
    public class Chart
    {
        public string[] labels { get; set; }
        public int[] data { get; set; }
    }
}
