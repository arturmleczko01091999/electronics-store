﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElectronicsStore.Models
{
    public class SortProductsOption
    {
        [Required]
        [StringLength(20, MinimumLength = 0)]
        public string SortType { get; set; }

        [Required]
        [StringLength(200, MinimumLength = 1)]
        public string SortName { get; set; }
    }
}
