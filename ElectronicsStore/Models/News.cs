﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace ElectronicsStore.Models
{
    public class News
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NewsId { get; set; }

        [Required]
        [StringLength(600, MinimumLength = 5)]
        public string Title { get; set; }

        [Required]
        [MinLength(5)]
        public string Content { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [StringLength(200, MinimumLength = 1)]
        public string ImageTitle { get; set; }

        [Required]
        [MinLength(1)]
        public string ImageUrl { get; set; }

        public bool isRecommended { get; set; }
    }
}