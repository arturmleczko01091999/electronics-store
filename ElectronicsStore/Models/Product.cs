﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ElectronicsStore.Enums;

namespace ElectronicsStore.Models
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Kod produktu jest wymagany")]
        [Display(Name = "Kod produktu")]
        public string Code { get; set; }

        [Required(ErrorMessage = "Nazwa produktu jest wymagana")]
        [Display(Name = "Nazwa")]
        [StringLength(250, MinimumLength = 3)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Ilość dostępnych produktów jest wymagana")]
        [Display(Name = "Ilość")]
        [Range(0, int.MaxValue)]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "Cena produktu jest wymagana")]
        [Display(Name = "Cena netto")]
        [Range(0.1, Double.PositiveInfinity, ErrorMessage = "Cena musi być wyższa niż 0 zł")]
        public decimal Price { get; set; }

        [Range(0.1, Double.PositiveInfinity, ErrorMessage = "Poprzednia cena musi być wyższa niż 0 zł")]
        [Display(Name = "Poprzednia cena netto")]
        public decimal? OldPrice { get; set; }

        public string Description { get; set; }

        public string Information { get; set; }

        public string Characteristics { get; set; }

        public bool IsMostPopular { get; set; }

        public SpecialOffer SpecialOffer { get; set; } = SpecialOffer.None;

        [Required]
        [Display(Name = "Kategoria")]
        public int CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public Category Category { get; set; }

        public List<ProductImage> Images { get; set; }
    }
}