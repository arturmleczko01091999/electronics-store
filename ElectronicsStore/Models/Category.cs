﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElectronicsStore.Models
{
    public class Category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CategoryId { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 1)]
        public string Name { get; set; }

        [StringLength(200, MinimumLength = 1)]
        public string ImageTitle { get; set; }

        [Required]
        [MinLength(1)]
        public string ImageUrl { get; set; }

        public List<Product> Products { get; set; }
    }
}
