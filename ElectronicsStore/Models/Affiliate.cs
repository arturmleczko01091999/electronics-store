﻿namespace ElectronicsStore.Models
{
    public class Affiliate
    {
        public int AffiliateId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
    }
}