﻿namespace ElectronicsStore.Models
{
    public class Country
    {
        public string name_pl { get; set; }
        public string name_en { get; set; }
        public string code { get; set; }
    }
}
