﻿using System;
using System.Collections.Generic;

namespace ElectronicsStore.Models
{
    public class SortProducts
    {
        public SortProductsOption CurrentSortingOption { get; set; }
        public List<SortProductsOption> SortProductsOptions { get; set; }
    }
}
