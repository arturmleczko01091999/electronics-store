﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElectronicsStore.Models
{
    public class Banner
    {
        public int BannerId { get; set; }
        public string ImageTitle { get; set; }
        public string ImageUrl { get; set; }
    }
}
