﻿using System.Collections.Generic;

namespace ElectronicsStore.Models
{
    public class NewsPager
    {
        public List<News> News { get; set; }
        public Pager Pager { get; set; }
    }
}
