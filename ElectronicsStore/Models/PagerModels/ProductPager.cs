﻿using System.Collections.Generic;

namespace ElectronicsStore.Models
{
    public class ProductPager
    {
        public List<Product> Products { get; set; }
        public Pager Pager { get; set; }
    }
}
