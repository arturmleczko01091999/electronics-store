#pragma checksum "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/_Menu.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "48e8ce4b7c8f290ceeb9a2ad90a9ea39c318b502"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Partials_Admin__Menu), @"mvc.1.0.view", @"/Views/Partials/Admin/_Menu.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using System.Globalization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using System.Text.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore.Enums;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore.Utility;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"48e8ce4b7c8f290ceeb9a2ad90a9ea39c318b502", @"/Views/Partials/Admin/_Menu.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ddc876db17b799c4ab8df277968cea949a45313e", @"/Views/_ViewImports.cshtml")]
    public class Views_Partials_Admin__Menu : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Assortment", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Products", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("accordion-body--item-link"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Categories", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "AddProduct", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Article", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Articles", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "AddArticle", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/_Menu.cshtml"
  
    string contoller = ViewContext.RouteData.Values["controller"].ToString();
    string action = ViewContext.RouteData.Values["action"].ToString();

    string path = $"{contoller}/{action}";

#line default
#line hidden
#nullable disable
            WriteLiteral("\n<div class=\"accordion\" id=\"accordionPanelsStayOpenExample\">\n    <div class=\"accordion-item\">\n        <h2 class=\"accordion-header\" id=\"panelsStayOpen-headingOne\">\n            <button");
            BeginWriteAttribute("class", " class=\"", 380, "\"", 452, 2);
            WriteAttributeValue("", 388, "accordion-button", 388, 16, true);
#nullable restore
#line 11 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/_Menu.cshtml"
WriteAttributeValue(" ", 404, contoller == "Assortment" ? "" : "collapsed", 405, 47, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" type=""button"" data-bs-toggle=""collapse"" data-bs-target=""#panelsStayOpen-collapseOne"" aria-expanded=""true"" aria-controls=""panelsStayOpen-collapseOne"">
                <div class=""accordion-button--icon assortment-icon""></div>
                <span>Asortyment</span>
            </button>
        </h2>
        <div id=""panelsStayOpen-collapseOne""");
            BeginWriteAttribute("class", " class=\"", 799, "\"", 877, 3);
            WriteAttributeValue("", 807, "accordion-collapse", 807, 18, true);
            WriteAttributeValue(" ", 825, "collapse", 826, 9, true);
#nullable restore
#line 16 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/_Menu.cshtml"
WriteAttributeValue(" ", 834, contoller == "Assortment" ? "show" : "", 835, 42, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" aria-labelledby=\"panelsStayOpen-headingOne\">\n            <div class=\"accordion-body\">\n                <ul>\n                    <li");
            BeginWriteAttribute("class", " class=\"", 1009, "\"", 1086, 2);
            WriteAttributeValue("", 1017, "accordion-body--item", 1017, 20, true);
#nullable restore
#line 19 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/_Menu.cshtml"
WriteAttributeValue(" ", 1037, path == "Assortment/Products" ? "active" : "", 1038, 48, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\n                        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "48e8ce4b7c8f290ceeb9a2ad90a9ea39c318b5028940", async() => {
                WriteLiteral("Produkty");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\n                    </li>\n                    <li");
            BeginWriteAttribute("class", " class=\"", 1262, "\"", 1341, 2);
            WriteAttributeValue("", 1270, "accordion-body--item", 1270, 20, true);
#nullable restore
#line 22 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/_Menu.cshtml"
WriteAttributeValue(" ", 1290, path == "Assortment/Categories" ? "active" : "", 1291, 50, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\n                        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "48e8ce4b7c8f290ceeb9a2ad90a9ea39c318b50210933", async() => {
                WriteLiteral("Kategorie produktów");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\n                    </li>\n                    <li");
            BeginWriteAttribute("class", " class=\"", 1530, "\"", 1609, 2);
            WriteAttributeValue("", 1538, "accordion-body--item", 1538, 20, true);
#nullable restore
#line 25 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/_Menu.cshtml"
WriteAttributeValue(" ", 1558, path == "Assortment/AddProduct" ? "active" : "", 1559, 50, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\n                        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "48e8ce4b7c8f290ceeb9a2ad90a9ea39c318b50212938", async() => {
                WriteLiteral("\n                            <i class=\"fas fa-plus me-1\"></i>\n                            <span>Dodaj produkt</span>\n                        ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\n                    </li>\n                </ul>\n            </div>\n        </div>\n    </div>\n    <div class=\"accordion-item\">\n        <h2 class=\"accordion-header\" id=\"panelsStayOpen-headingTwo\">\n            <button");
            BeginWriteAttribute("class", " class=\"", 2085, "\"", 2154, 2);
            WriteAttributeValue("", 2093, "accordion-button", 2093, 16, true);
#nullable restore
#line 37 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/_Menu.cshtml"
WriteAttributeValue(" ", 2109, contoller == "Article" ? "" : "collapsed", 2110, 44, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" type=""button"" data-bs-toggle=""collapse"" data-bs-target=""#panelsStayOpen-collapseTwo"" aria-expanded=""false"" aria-controls=""panelsStayOpen-collapseTwo"">
                <div class=""accordion-button--icon article-icon""></div>
                <span>Artykuły</span>
            </button>
        </h2>
        <div id=""panelsStayOpen-collapseTwo""");
            BeginWriteAttribute("class", " class=\"", 2497, "\"", 2572, 3);
            WriteAttributeValue("", 2505, "accordion-collapse", 2505, 18, true);
            WriteAttributeValue(" ", 2523, "collapse", 2524, 9, true);
#nullable restore
#line 42 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/_Menu.cshtml"
WriteAttributeValue(" ", 2532, contoller == "Article" ? "show" : "", 2533, 39, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" aria-labelledby=\"panelsStayOpen-headingTwo\">\n            <div class=\"accordion-body\">\n                <ul>\n                    <li");
            BeginWriteAttribute("class", " class=\"", 2704, "\"", 2778, 2);
            WriteAttributeValue("", 2712, "accordion-body--item", 2712, 20, true);
#nullable restore
#line 45 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/_Menu.cshtml"
WriteAttributeValue(" ", 2732, path == "Article/Articles" ? "active" : "", 2733, 45, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\n                        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "48e8ce4b7c8f290ceeb9a2ad90a9ea39c318b50216767", async() => {
                WriteLiteral("Artykuły");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_6.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_6);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\n                    </li>\n                    <li");
            BeginWriteAttribute("class", " class=\"", 2951, "\"", 3027, 2);
            WriteAttributeValue("", 2959, "accordion-body--item", 2959, 20, true);
#nullable restore
#line 48 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/_Menu.cshtml"
WriteAttributeValue(" ", 2979, path == "Article/AddArticle" ? "active" : "", 2980, 47, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\n                        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "48e8ce4b7c8f290ceeb9a2ad90a9ea39c318b50218758", async() => {
                WriteLiteral("\n                            <i class=\"fas fa-plus me-1\"></i>\n                            <span>Dodaj artykuł</span>\n                        ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_7.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_7);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\n                    </li>\n                </ul>\n            </div>\n        </div>\n    </div>\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
