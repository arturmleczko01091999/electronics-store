#pragma checksum "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/Articles/EditArticle/_Publication.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b4a32c6951a087ca78323087af6cb98c17ab7d0c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Partials_Admin_Articles_EditArticle__Publication), @"mvc.1.0.view", @"/Views/Partials/Admin/Articles/EditArticle/_Publication.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using System.Globalization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using System.Text.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore.Enums;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore.Utility;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b4a32c6951a087ca78323087af6cb98c17ab7d0c", @"/Views/Partials/Admin/Articles/EditArticle/_Publication.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ddc876db17b799c4ab8df277968cea949a45313e", @"/Views/_ViewImports.cshtml")]
    public class Views_Partials_Admin_Articles_EditArticle__Publication : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ElectronicsStore.Models.News>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"
<div class=""assortment-card article-card-container article-publication-card-container"">
    <h3 class=""assortment-heading"">Publikacja</h3>
    <div class=""form-group-article--recommended"">
        <p class=""form-label form-label--article"">Polacany</p>
        <div class=""form-check form-check--article-list form-switch"">
");
#nullable restore
#line 8 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/Articles/EditArticle/_Publication.cshtml"
             if (Model.isRecommended)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <input class=\"form-check-input js-articleRecommendedSwitch\" checked type=\"checkbox\" role=\"switch\">\n");
#nullable restore
#line 11 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/Articles/EditArticle/_Publication.cshtml"
            }
            else
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <input class=\"form-check-input js-articleRecommendedSwitch\" type=\"checkbox\" role=\"switch\">\n");
#nullable restore
#line 15 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/Articles/EditArticle/_Publication.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </div>\n    </div>\n    <div class=\"form-group-article--publication-date\">\n        <p class=\"form-label form-label--article\">Data publikacji</p>\n        <span class=\"publiaction-date\">");
#nullable restore
#line 20 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/Articles/EditArticle/_Publication.cshtml"
                                  Write(Model.Date.Day.ToString("00"));

#line default
#line hidden
#nullable disable
            WriteLiteral(" ");
#nullable restore
#line 20 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/Articles/EditArticle/_Publication.cshtml"
                                                                 Write(Model.Date.ToString("MMMM", CultureInfo.CreateSpecificCulture("pl")));

#line default
#line hidden
#nullable disable
            WriteLiteral(" ");
#nullable restore
#line 20 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/Admin/Articles/EditArticle/_Publication.cshtml"
                                                                                                                                       Write(Model.Date.Year);

#line default
#line hidden
#nullable disable
            WriteLiteral("</span>\n    </div>\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ElectronicsStore.Models.News> Html { get; private set; }
    }
}
#pragma warning restore 1591
