#pragma checksum "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/ProductCard/_Description.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b8af1265c181117fe9b506c7d8be95e8383441b3"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Partials_ProductCard__Description), @"mvc.1.0.view", @"/Views/Partials/ProductCard/_Description.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using System.Globalization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using System.Text.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore.Enums;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore.Utility;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b8af1265c181117fe9b506c7d8be95e8383441b3", @"/Views/Partials/ProductCard/_Description.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ddc876db17b799c4ab8df277968cea949a45313e", @"/Views/_ViewImports.cshtml")]
    public class Views_Partials_ProductCard__Description : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ElectronicsStore.Models.Product>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\n<div class=\"card-container product-card-container product-card-description-container\">\n    <h3 class=\"product-card-heading\">Opis produktu</h3>\n    <div class=\"product-card-description-content\">\n        ");
#nullable restore
#line 6 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/ProductCard/_Description.cshtml"
   Write(Html.Raw(Model.Description));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n    </div>\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ElectronicsStore.Models.Product> Html { get; private set; }
    }
}
#pragma warning restore 1591
