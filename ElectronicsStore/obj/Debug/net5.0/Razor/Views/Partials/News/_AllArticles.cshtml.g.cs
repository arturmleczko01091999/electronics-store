#pragma checksum "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0e4cef9cb2d59064175a6d6f2ad6b72913d9a5a1"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Partials_News__AllArticles), @"mvc.1.0.view", @"/Views/Partials/News/_AllArticles.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using System.Globalization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using System.Text.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore.Enums;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore.Utility;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0e4cef9cb2d59064175a6d6f2ad6b72913d9a5a1", @"/Views/Partials/News/_AllArticles.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ddc876db17b799c4ab8df277968cea949a45313e", @"/Views/_ViewImports.cshtml")]
    public class Views_Partials_News__AllArticles : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ElectronicsStore.Models.NewsPager>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/icons/arrow-right-primary.svg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("Arrow right"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Article", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("abridged-article card-container"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Article", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "AddArticle", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("no-articles-info--add"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\n<div");
            BeginWriteAttribute("class", " class=\"", 46, "\"", 118, 2);
            WriteAttributeValue("", 54, "all-articles-container", 54, 22, true);
#nullable restore
#line 3 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
WriteAttributeValue(" ", 76, @Model.News.Count() < 4 ? " mb-5" : "", 77, 41, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\n");
#nullable restore
#line 4 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
     if (Model.News.Count() > 0)
    {
        

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
         foreach (News news in Model.News)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0e4cef9cb2d59064175a6d6f2ad6b72913d9a5a17399", async() => {
                WriteLiteral("\n                <img");
                BeginWriteAttribute("src", " src=\"", 338, "\"", 371, 1);
#nullable restore
#line 9 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
WriteAttributeValue("", 344, Url.Content(news.ImageUrl), 344, 27, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                BeginWriteAttribute("alt", " alt=\"", 372, "\"", 394, 1);
#nullable restore
#line 9 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
WriteAttributeValue("", 378, news.ImageTitle, 378, 16, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"abridged-article--picture\" />\n                <div class=\"abridged-article--content\">\n                    <h5 class=\"abridged-article--content-date\">");
#nullable restore
#line 11 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
                                                          Write(news.Date.ToString("d"));

#line default
#line hidden
#nullable disable
                WriteLiteral("</h5>\n                    <h2 class=\"abridged-article--content-title\">");
#nullable restore
#line 12 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
                                                           Write(news.Title);

#line default
#line hidden
#nullable disable
                WriteLiteral("</h2>\n                    <p class=\"abridged-article--content-text\">");
#nullable restore
#line 13 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
                                                         Write(HtmlAgilityHelper.HtmlToPlainText(news.Content));

#line default
#line hidden
#nullable disable
                WriteLiteral("</p>\n                    <div class=\"abridged-article--content-read-more\">\n                        <span>Czytaj dalej</span>\n                        ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "0e4cef9cb2d59064175a6d6f2ad6b72913d9a5a19792", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\n                    </div>\n                </div>\n            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 8 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
                                      WriteLiteral(news.NewsId);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\n");
#nullable restore
#line 20 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
        }

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
         
    }
    else
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div class=\"card-container no-articles-info-container\">\n            <p class=\"no-articles-info\">Brak aktualności</p>\n");
#nullable restore
#line 26 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
             if (User.IsInRole(RoleHelper.Admin))
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0e4cef9cb2d59064175a6d6f2ad6b72913d9a5a113822", async() => {
                WriteLiteral("\n                    <span class=\"no-articles-info--add-text\">Dodaj artykuł</span>\n                    <img class=\"no-articles-info--add-icon\" />\n                ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\n");
#nullable restore
#line 32 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </div>\n");
#nullable restore
#line 34 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("\n");
#nullable restore
#line 36 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
     if (Model.News.Count() > 3)
    {

        

#line default
#line hidden
#nullable disable
#nullable restore
#line 39 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
   Write(await Html.PartialAsync("/Views/Partials/News/_Pager.cshtml", Model));

#line default
#line hidden
#nullable disable
#nullable restore
#line 39 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Partials/News/_AllArticles.cshtml"
                                                                             
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ElectronicsStore.Models.NewsPager> Html { get; private set; }
    }
}
#pragma warning restore 1591
