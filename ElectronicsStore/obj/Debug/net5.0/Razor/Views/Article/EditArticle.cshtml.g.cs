#pragma checksum "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Article/EditArticle.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "85861e6e2a704a2153ff8ae68cd12bac75b2e0da"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Article_EditArticle), @"mvc.1.0.view", @"/Views/Article/EditArticle.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using System.Globalization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using System.Text.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore.Enums;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/_ViewImports.cshtml"
using ElectronicsStore.Utility;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"85861e6e2a704a2153ff8ae68cd12bac75b2e0da", @"/Views/Article/EditArticle.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ddc876db17b799c4ab8df277968cea949a45313e", @"/Views/_ViewImports.cshtml")]
    public class Views_Article_EditArticle : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ElectronicsStore.Models.News>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("name", "/Views/Partials/Admin/_Menu.cshtml", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.PartialTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\n<div class=\"admin-container\">\n    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("partial", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "85861e6e2a704a2153ff8ae68cd12bac75b2e0da4227", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.PartialTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_PartialTagHelper.Name = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\n    <div data-article-id=\"");
#nullable restore
#line 5 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Article/EditArticle.cshtml"
                     Write(Model.NewsId);

#line default
#line hidden
#nullable disable
            WriteLiteral("\">\n        ");
#nullable restore
#line 6 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Article/EditArticle.cshtml"
   Write(await Html.PartialAsync("/Views/Partials/Admin/Articles/EditArticle/_Toolbar.cshtml", Model));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        <div class=\"admin-container-body article-content-wrapper\">\n            ");
#nullable restore
#line 8 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Article/EditArticle.cshtml"
       Write(await Html.PartialAsync("/Views/Partials/Admin/Articles/EditArticle/_Basics.cshtml", Model));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n            ");
#nullable restore
#line 9 "/Users/arturmleczko/Desktop/ElectronicsStore/ElectronicsStore/Views/Article/EditArticle.cshtml"
       Write(await Html.PartialAsync("/Views/Partials/Admin/Articles/EditArticle/_Publication.cshtml", Model));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n        </div>\n    </div>\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ElectronicsStore.Models.News> Html { get; private set; }
    }
}
#pragma warning restore 1591
