﻿namespace ElectronicsStore.Enums
{
    public enum SpecialOffer
    {
        None = 0,
        Promotion = 1,
        New = 2,
        Outlet = 3
    }
}
