﻿const origin = window.location.origin;

export async function deleteProduct(productId) {
    const url = `${origin}/api/products/${productId}`;

    try {
        const res = await fetch(url, { method: 'DELETE' });
        const json = await res.json();
        return json;
    } catch (error) {
        return {};
    }
}
