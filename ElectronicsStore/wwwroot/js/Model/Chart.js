﻿const origin = window.location.origin;

export async function getCategoryDoughnutChart() {
    const url = `${origin}/api/categories/products/chart`;

    try {
        const res = await fetch(url);
        const json = await res.json();
        return json
    } catch (error) {
        return {};
    }
}