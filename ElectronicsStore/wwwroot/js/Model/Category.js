﻿const origin = window.location.origin;

const url = `${origin}/api/categories`;

export async function addCategory(data) {
    try {
        const res = await axios.post(url, data);
        return res.data;
    } catch (error) {
        return {};
    }
}

export async function updateCategory(categoryId, data) {
    try {
        const res = await axios.put(`${url}/${categoryId}`, data);
        return res.data;
    } catch (error) {
        return {};
    }
}

export async function deleteCategory(categoryId) {
    try {
        const res = await axios.delete(`${url}/${categoryId}`);
        return res.data;
    } catch (error) {
        return {};
    }
}