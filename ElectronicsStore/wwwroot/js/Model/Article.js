﻿const origin = window.location.origin;

const url = `${origin}/api/articles`;

export async function addArticle(data) {
    try {
        const res = await axios.post(url, data);
        return res.data;
    } catch (error) {
        return {};
    }
}

export async function updateArticle(articleId, data) {
    try {
        const res = await axios.put(`${url}/${articleId}`, data);
        return res.data;
    } catch (error) {
        return {};
    }
}

export async function updateArticleContent(articleId, data) {
    try {
        const res = await axios.patch(`${url}/${articleId}`, data);
        return res.data;
    } catch (error) {
        return {};
    }
}

export async function deleteArticle(articleId) {
    try {
        const res = await axios.delete(`${url}/${articleId}`);
        return res.data;
    } catch (error) {
        return {};
    }
}
