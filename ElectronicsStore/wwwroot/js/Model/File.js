﻿const origin = window.location.origin;

export async function addFile(directory, data) {
    const url = `${origin}/api/files/${directory}`;

    try {
        const res = await axios.post(url, data);
        return res.data;
    } catch (error) {
        return {};
    }
}