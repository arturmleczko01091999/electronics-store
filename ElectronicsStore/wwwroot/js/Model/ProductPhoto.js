﻿const origin = window.location.origin;

export async function deleteProductPhoto(productPhotoId) {
    const url = `${origin}/api/products/photos/${productPhotoId}`;

    try {
        const res = await fetch(url, { method: 'DELETE' });
        const json = await res.json();
        return json;
    } catch (error) {
        return {};
    }
}

export async function updateProductPhoto(productPhotoId, data) {
    const url = `${origin}/api/products/photos/${productPhotoId}`;

    try {
        const res = await fetch(url,
            {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: data
            });
        const json = await res.json();
        return json;
    } catch (error) {
        return {};
    }
}