﻿export function mod(modName, comparisonType = 'include') {
    const pathName = window.location.pathname;

    if (comparisonType === 'include') {
        if (pathName.includes(modName)) {
            return true;
        } else {
            return false;
        }
    } else if (comparisonType === 'equal'){
        if (pathName === modName) {
            return true;
        } else {
            return false;
        }
    }
}