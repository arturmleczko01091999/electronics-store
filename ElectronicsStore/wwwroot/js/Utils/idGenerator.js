﻿export function idGenerator(idLength) {
    let resultingId = '';

    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;

    for (let characterIndex = 0; characterIndex < idLength; characterIndex++) {
        resultingId += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return resultingId;
}