﻿export class ToasterController {
    sel = {
        errorElement: '.validation-summary-errors',
        toast: '.toast',
        toastBtnClose: '.toast-btn-close'
    }

    errorMessage;

    init = () => {
        this.handleErrorElement();

        if (this.errorMessage) {
            this.renderToastElement();
        }
    }

    handleErrorElement = () => {
        const errorElement = document.querySelector(this.sel.errorElement);

        if (errorElement !== null) {
            this.hideErrorElement(errorElement)
            this.getErrorMessage(errorElement);
        }
    }

    hideErrorElement = (errorElement) => {
        errorElement.classList.add('d-none');
    }

    getErrorMessage = (errorElement) => {
        const errorMessage = errorElement.innerText;
        this.errorMessage = errorMessage;
    }

    renderToastElement = (errorMessage = this.errorMessage) => {
        const toastHTML = `
            <div class="toast show fade" autohide="true" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-content">
                    <img src="/icons/danger.svg" alt="Ostrzeżenie" class="toast-content--icon"/>
                    <div class="toast-content-text">
                        <h6 class="toast-content--heading">Ostrzeżenie</h6>
                        <span class="toast-content--description">${errorMessage}</span>
                    </div>
                    <button type="button" class="btn-close toast-btn-close ms-auto" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
            </div>
        `;

        document.body.insertAdjacentHTML('beforeend', toastHTML);

        this.closeToastManual();
        this.closeToastAutomatically();
    }

    closeToastAutomatically = () => {
        setTimeout(() => this.closeToast(), 5000)
    }

    closeToastManual = () => {
        const toastBtnClose = document.querySelector(this.sel.toastBtnClose);
        toastBtnClose.addEventListener('click', this.closeToast);
    }

    closeToast = () => {
        const toast = document.querySelector(this.sel.toast);
        toast.classList.replace("show", "hide");
    }
}