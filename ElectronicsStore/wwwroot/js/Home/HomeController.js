﻿export class HomeController {
    sel = {
        bannerSlider: '.banner-slider',
        mostPopularSlider: '.most-popular-slider',
        outletSlider: '.outlet-slider',
        bannerNavigation: '.banner-navigation',
        bannerArrowNext: '.banner-arrow-next',
        bannerArrorPrev: '.banner-arrow-prev',
        mostPopularArrowNext: '.most-popular-arrow-next',
        mostPopularArrowPrev: '.most-popular-arrow-prev',
        outletArrowNext: '.outlet-arrow-next',
        outletArrowPrev: '.outlet-arrow-prev',
        bannerPagination: '.swiper-pagination'
    };

    elements = {
        bannerNavigation: document.querySelector(this.sel.bannerNavigation)
    }

    numberOfSlides = window.innerWidth > 1400 ? 4 : 3;

    bannerSlider;
    mostPopularSlider;
    outletSlider;

    init = () => {
        this.setBannerSlider();
        this.controlBannerSlider();
        this.setMostPopularSlider();
        this.setOutletSlider();
    }

    setBannerSlider = () => {
        this.bannerSlider = new Swiper(this.sel.bannerSlider, {
            navigation: {
                nextEl: this.sel.bannerArrowNext,
                prevEl: this.sel.bannerArrorPrev
            },
            pagination: {
                el: this.sel.bannerPagination,
                type: 'fraction',
            },
            autoplay: {
                delay: 5000
            },
            loop: true
        });
    }

    controlBannerSlider = () => {
        this.elements.bannerNavigation.addEventListener('mouseenter', () => {
            this.bannerSlider.autoplay.stop();
        });

        this.elements.bannerNavigation.addEventListener('mouseleave', () => {
            this.bannerSlider.autoplay.start();
        });
    }

    setMostPopularSlider = () => {
        this.mostPopularSlider = new Swiper(this.sel.mostPopularSlider, {
            slidesPerView: this.numberOfSlides,
            spaceBetween: 30,
            slidesPerGroup: this.numberOfSlides,
            loop: true,
            loopFillGroupWithBlank: true,
            navigation: {
                nextEl: this.sel.mostPopularArrowNext,
                prevEl: this.sel.mostPopularArrowPrev
            },
        })
    }

    setOutletSlider = () => {
        this.outletSlider = new Swiper(this.sel.outletSlider, {
            slidesPerView: this.numberOfSlides,
            spaceBetween: 30,
            slidesPerGroup: this.numberOfSlides,
            loop: true,
            loopFillGroupWithBlank: true,
            navigation: {
                nextEl: this.sel.outletArrowNext,
                prevEl: this.sel.outletArrowPrev
            },
        })
    }
}




