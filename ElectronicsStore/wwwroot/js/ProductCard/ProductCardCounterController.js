﻿export class ProductCardCounterController {
    sel = {
        productCardCounter: '.product-card-counter--input',
        productCardCounterIncrementButton: '.product-card-counter--increment-button',
        productCardCounterDecrementButton: '.product-card-counter--decrement-button'
    }

    elements = {
        productCardCounter: document.querySelector(this.sel.productCardCounter),
        productCardCounterIncrementButton: document.querySelector(this.sel.productCardCounterIncrementButton),
        productCardCounterDecrementButton: document.querySelector(this.sel.productCardCounterDecrementButton)
    }

    init = () => {
        this.setCounterValue();
    }

    setCounterValue = () => {
        const counterMaxValue = Number(this.elements.productCardCounter.max);

        this.elements.productCardCounterIncrementButton.addEventListener('click', () => {
            this.incrementCounterValue(counterMaxValue);
        })

        this.elements.productCardCounterDecrementButton.addEventListener('click', this.decrementCounerValue)
    }

    incrementCounterValue = (counterMaxValue) => {
        const counterValue = this.elements.productCardCounter.value;

        if (counterValue < counterMaxValue) {
            this.elements.productCardCounter.value++;
        }
    }

    decrementCounerValue = () => {
        const counterValue = this.elements.productCardCounter.value;

        if (counterValue > 1) {
            this.elements.productCardCounter.value--;
        }
    }
}