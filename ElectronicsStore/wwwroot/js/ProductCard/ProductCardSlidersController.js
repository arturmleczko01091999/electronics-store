﻿export class ProductCardSlidersController {
    sel = {
        productCardSlider: '.product-card-slider',
        productCardSliderThumbs: '.product-card-slider--thumbs',
        productCardSliderArrowPrev: '.product-card-slider-arrow-prev',
        productCardSliderArrowNext: '.product-card-slider-arrow-next',
        productCardSliderWrapper: '.product-card-slider .swiper-wrapper',
        fullscreenSlider: '.fullscreen-slider',
        fullscreenSliderThumbs: '.fullscreen-slider--thumbs',
        fullscreenSliderContainer: '.fullscreen-slider-container',
        fullscreenSliderCloseButton: '.fullscreen-slider-close',
        fullscreenSliderPagination: '.fullscreen-slider-pagination',
        fullscreenSliderArrowPrev: '.fullscreen-slider-arrow-prev',
        fullscreenSliderArrowNext: '.fullscreen-slider-arrow-next',
        similarProductsSlider: '.similar-products-slider',
        similarProductsArrowNext: '.similar-products-arrow-next',
        similarProductsArrowPrev: '.similar-products-arrow-prev',
        tabContentPhotoBtnZoom: '.tab-content-photo-btn--zoom',
    };

    elements = {
        productCardSliderWrapper: document.querySelector(this.sel.productCardSliderWrapper),
        fullscreenSliderContainer: document.querySelector(this.sel.fullscreenSliderContainer),
        fullscreenSliderCloseButton: document.querySelector(this.sel.fullscreenSliderCloseButton),
        tabContentPhotoButtonsZoom: document.querySelectorAll(this.sel.tabContentPhotoBtnZoom)
    }

    productCardSlider;
    productCardSliderThumbs;
    fullscreenSlider;
    fullscreenSliderThumbs;

    productCardSliderRealIndex = 0;

    init = () => {
        this.setProductCardSliderThumbs();
        this.setProductCardSlider();
        this.setSimilarProductsSlider();
        this.handleProductCardSliderWrapper();
        this.getProductCardSliderRealIndex();
        this.handleFullscreenCloseButton();
        this.setFullscreenSliderThumbs();
        this.setFullscreenSlider();
        this.handleTabContentPhotoButtonZoom();
    }

    handleProductCardSliderWrapper = () => {
        this.elements.productCardSliderWrapper.addEventListener('click', this.openFullscreenSliderContainer);
    }

    handleFullscreenCloseButton = () => {
        this.elements.fullscreenSliderCloseButton.addEventListener('click', this.closeFullscreenSliderContainer);
    }

    handleTabContentPhotoButtonZoom = () => {
        this.elements.tabContentPhotoButtonsZoom.forEach((button, index) => {
            button.addEventListener('click', () => {
                this.zoomPhoto(index);
            });
        })
    }

    setProductCardSliderThumbs = () => {
        this.productCardSliderThumbs = new Swiper(this.sel.productCardSliderThumbs, {
            spaceBetween: 10,
            slidesPerView: 4,
            freeMode: true,
            watchSlidesProgress: true
        })
    }

    setProductCardSlider = () => {
        this.productCardSlider = new Swiper(this.sel.productCardSlider, {
            spaceBetween: 10,
            navigation: {
                nextEl: this.sel.productCardSliderArrowNext,
                prevEl: this.sel.productCardSliderArrowPrev
            },
            thumbs: {
                swiper: this.productCardSliderThumbs
            }
        })
    }

    setFullscreenSliderThumbs = () => {
        this.fullscreenSliderThumbs = new Swiper(this.sel.fullscreenSliderThumbs, {
            spaceBetween: 10,
            slidesPerView: 4,
            freeMode: true,
            watchSlidesProgress: true
        })
    }

    setFullscreenSlider = () => {
        this.fullscreenSlider = new Swiper(this.sel.fullscreenSlider, {
            spaceBetween: 10,
            initialSlide: this.productCardSliderRealIndex,
            navigation: {
                nextEl: this.sel.fullscreenSliderArrowNext,
                prevEl: this.sel.fullscreenSliderArrowPrev
            },
            pagination: {
                el: this.sel.fullscreenSliderPagination,
                type: "fraction",
            },
            thumbs: {
                swiper: this.fullscreenSliderThumbs
            }
        })
    }

    setSimilarProductsSlider = () => {
        this.mostPopularSlider = new Swiper(this.sel.similarProductsSlider, {
            slidesPerView: 3,
            spaceBetween: 16,
            slidesPerGroup: 3,
            loop: true,
            loopFillGroupWithBlank: true,
            navigation: {
                nextEl: this.sel.similarProductsArrowNext,
                prevEl: this.sel.similarProductsArrowPrev
            },
        })
    }

    openFullscreenSliderContainer = () => {
        this.elements.fullscreenSliderContainer.classList.add('active');
        this.setFullscreenSlider();
        document.body.classList.add('overflow-hidden');
    }

    closeFullscreenSliderContainer = () => {
        this.elements.fullscreenSliderContainer.classList.remove('active');
        document.body.classList.remove('overflow-hidden');
    }

    getProductCardSliderRealIndex = () => {
        this.productCardSlider.on('transitionEnd', () => {
            this.productCardSliderRealIndex = this.productCardSlider.realIndex;
        });
    }

    zoomPhoto = (index) => {
        console.log('dziala');
        this.productCardSliderRealIndex = index;
        this.openFullscreenSliderContainer();
    }
}