﻿export class ProductCardInformationAndPicturesController {
    sel = {
        informationTabContainer: '.information-tab-container',
        informationTabContent: '.tab-content-information',
        photosTabContainer: '.photos-tab-container',
        photosTabContent: '.tab-content-photos'
    };

    elements = {
        informationTabContainer: document.querySelector(this.sel.informationTabContainer),
        informationTabContent: document.querySelector(this.sel.informationTabContent),
        photosTabContainer: document.querySelector(this.sel.photosTabContainer),
        photosTabContent: document.querySelector(this.sel.photosTabContent)
    }

    init = () => {
        this.handleTabs();
    }

    handleTabs = () => {
        this.elements.informationTabContainer.addEventListener('click', this.activeInformationTab);
        this.elements.photosTabContainer.addEventListener('click', this.activePhotosTab);
    }

    activeInformationTab = () => {
        this.clearActivationClasses();
        this.elements.informationTabContainer.classList.add('active');
        this.elements.photosTabContainer.classList.add('inactive');

        this.changeTab('information');
    }

    activePhotosTab = () => {
        this.clearActivationClasses();
        this.elements.photosTabContainer.classList.add('active');
        this.elements.informationTabContainer.classList.add('inactive');

        this.changeTab('photos');
    }

    clearActivationClasses = () => {
        const { informationTabContainer, photosTabContainer } = this.elements;
        const tabContainers = [informationTabContainer, photosTabContainer];

        tabContainers.forEach(tabContainer => {
            const tabContainerClassName = tabContainer.className;

            if (tabContainerClassName.includes('active')) {
                tabContainer.classList.remove('active');
            }

            if (tabContainerClassName.includes('inactive')) {
                tabContainer.classList.remove('inactive');
            }
        })
    }

    changeTab = (tabName) => {
        const informationTabContentClass = this.sel.informationTabContent.substring(1);
        const photosTabContentClass = this.sel.photosTabContent.substring(1);

        if (tabName === 'information') {
            const informationClassName = informationTabContentClass;
            const photosClassName = `${photosTabContentClass} d-none`;

            this.elements.informationTabContent.className = informationClassName;
            this.elements.photosTabContent.className = photosClassName;
        } else {
            const informationClassName = `${informationTabContentClass} d-none`;
            const photosClassName = photosTabContentClass;

            this.elements.informationTabContent.className = informationClassName;
            this.elements.photosTabContent.className = photosClassName;
        }
    }
}