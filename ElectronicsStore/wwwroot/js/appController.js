﻿import { mod } from './Utils/mod.js';

import { ToasterController } from './ToasterController.js';
import { HomeController } from './Home/homeController.js';
import { LoginController } from './Account/loginController.js';
import { RegisterController } from './Account/RegisterController.js';
import { CategoryMenuController } from './Layout/CategoryMenuController.js';
import { ProductsController } from './Products/ProductsController.js';
import { ProductCardSlidersController } from './ProductCard/ProductCardSlidersController.js';
import { ProductCardCounterController } from './ProductCard/ProductCardCounterController.js';
import { ProductCardInformationAndPicturesController } from './ProductCard/ProductCardInformationAndPicturesController.js';
import { CategoryDoughnutChartController } from './Assortment/CategoryDoughnutChartController.js';
import { ProductBasicsController } from './Assortment/ProductBasicsController.js';
import { ProductCharacteristicsController } from './Assortment/ProductCharacteristicsController.js';
import { AddProductPhotoController } from './Assortment/AddProductPhotoController.js';
import { EditProductPhotoController } from './Assortment/EditProductPhotoController.js';
import { ProductScrollspyController } from './Assortment/ProductScrollspyController.js';
import { EditProductController } from './Assortment/EditProductController.js';
import { AddCategoryModalController } from './Assortment/Categories/AddCategoryModalController.js';
import { CategoriesController } from './Assortment/Categories/CategoriesController.js';
import { ArticleListController } from './Articles/ArticleListController.js';
import { ArticleController } from './Articles/ArticleController.js';

const toasterController = new ToasterController();
const homeController = new HomeController();
const loginController = new LoginController();
const registerController = new RegisterController();
const categoryMenuController = new CategoryMenuController();
const productsController = new ProductsController();
const productCardSlidersController = new ProductCardSlidersController();
const productCardCounterController = new ProductCardCounterController();
const productCardInformationAndPicturesController = new ProductCardInformationAndPicturesController();
const categoryDoughnutChartController = new CategoryDoughnutChartController();
const productBasicsController = new ProductBasicsController();
const productCharacteristicsController = new ProductCharacteristicsController();
const addProductPhotoController = new AddProductPhotoController();
const editProductPhotoController = new EditProductPhotoController();
const productScrollspyController = new ProductScrollspyController();
const editProductController = new EditProductController();
const addCategoryModalController = new AddCategoryModalController();
const categoriesController = new CategoriesController();
const articleListController = new ArticleListController();
const articleController = new ArticleController();

toasterController.init();
categoryMenuController.init();

if (mod('/', 'equal')) {
    homeController.init();
}

if (mod('Account/Login')) {
    loginController.init();
}

if (mod('Account/Register')) {
    registerController.init();
}

if (mod('Products/Category/')) {
    productsController.init();
}

if (mod('ProductCard/Index')) {
    productCardCounterController.init();
    productCardSlidersController.init();
    productCardInformationAndPicturesController.init();
}

if (mod('Assortment/Products')) {
    categoryDoughnutChartController.init();
}

if (mod('Assortment/AddProduct') || mod('Assortment/EditProduct')) {
    productBasicsController.init();
    productCharacteristicsController.init();
    productScrollspyController.init();
}

if (mod('Assortment/AddProduct')) {
    addProductPhotoController.init();
}

if (mod('Assortment/EditProduct')) {
    editProductController.init();
    editProductPhotoController.init();
}

if (mod('Assortment/Categories')) {
    addCategoryModalController.init();
    categoriesController.init();
}

if (mod('Article/Articles')) {
    articleListController.init();
}

if (mod('Article/AddArticle') || mod('Article/EditArticle')) {
    articleController.init();
}









