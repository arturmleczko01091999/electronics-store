﻿import { idGenerator } from '../Utils/idGenerator.js';
import { addArticle, updateArticle, deleteArticle } from '../Model/Article.js';
import { addFile } from '../Model/File.js';
import { ToasterController } from '../ToasterController.js';

export class ArticleController {
    currentAction = location.pathname.split("/")[2];

    sel = {
        articleTitleInput: '.js-articleTitleInput',
        numberCharactersToUseArticleName: '.js-numberCharactersToUseArticleName',
        articleImageUploader: '.js-articleImageUploader',
        articleImage: '.js-articleImage',
        articleImageWrapper: '.js-articleImageWrapper',
        saveArticleButton: '.js-saveArticleButton',
        deleteArticleButton: '.js-deleteArticleButton',
        articleImageAlternativeTitleInput: '.js-articleImageAlternativeTitleInput',
        articleRecommendedSwitch: '.js-articleRecommendedSwitch',
        articleElement: '[data-article-id]'
    }

    elements = {
        articleTitleInput: document.querySelector(this.sel.articleTitleInput),
        numberCharactersToUseArticleName: document.querySelector(this.sel.numberCharactersToUseArticleName),
        articleImageUploader: document.querySelector(this.sel.articleImageUploader),
        articleImage: document.querySelector(this.sel.articleImage),
        articleImageWrapper: document.querySelector(this.sel.articleImageWrapper),
        saveArticleButton: document.querySelector(this.sel.saveArticleButton),
        articleImageAlternativeTitleInput: document.querySelector(this.sel.articleImageAlternativeTitleInput),
        articleRecommendedSwitch: document.querySelector(this.sel.articleRecommendedSwitch),
        articleElement: document.querySelector(this.sel.articleElement)
    }

    toastController = new ToasterController();

    init = () => {
        this.setNumberCharactersToUseArticleTitle(this.elements.articleTitleInput);
        this.handleArticleTitleInput();
        this.handleArticleImageUploader();
        this.handleSaveArticleButton();

        if (this.currentAction === 'EditArticle') {
            this.handleDeleteArticleButton();
        }
    }

    handleArticleTitleInput = () => {
        this.elements.articleTitleInput.addEventListener('input', () => this.setNumberCharactersToUseArticleTitle(this.elements.articleTitleInput));
    }

    handleArticleImageUploader = () => {
        this.elements.articleImageUploader.addEventListener('change', (event) => this.setArticleImagePreview(event));
    }

    handleSaveArticleButton = () => {
        this.elements.saveArticleButton.addEventListener('click', this.saveArticle);
    }

    handleDeleteArticleButton = () => {
        const deleteArticleButton = document.querySelector(this.sel.deleteArticleButton);
        deleteArticleButton.addEventListener('click', this.deleteArticle);
    }

    setNumberCharactersToUseArticleTitle = (articleTitleInput) => {
        const maxArticleTitleLength = articleTitleInput.maxLength;
        const articleTitleInputLength = articleTitleInput.value.length;

        const numberCharactersToUseArticleTitle = maxArticleTitleLength - articleTitleInputLength;
        const kindOfLeft = numberCharactersToUseArticleTitle === 1 ? "Pozostał" : "Pozostało";
        const kindOfCharacters = numberCharactersToUseArticleTitle === 1 ? "znak" : "znaków";

        this.elements.numberCharactersToUseArticleName.innerText = `${kindOfLeft} ${numberCharactersToUseArticleTitle} ${kindOfCharacters}`;
    }

    setArticleImagePreview = (event) => {
        const { articleImageWrapper } = this.elements;

        const [file] = event.target.files;
        const articleImage = URL.createObjectURL(file);

        this.elements.articleImage.src = articleImage;

        if (articleImageWrapper.className.includes('empty')) {
            articleImageWrapper.classList.remove('empty');
        }
    }

    saveArticle = async () => {
        const { articleTitleInput, articleImageUploader, articleImageAlternativeTitleInput, articleRecommendedSwitch } = this.elements;
        const articleImagePreview = this.elements.articleImage;

        const articleTitle = articleTitleInput.value;
        const articleImage = articleImageUploader.files[0];
        const articleImagePreviewName = articleImagePreview.src ? articleImagePreview.src.split("/").pop() : null;
        const articleImageName = !!articleImage ? this.generateArticleImageName(articleImage.name) : articleImagePreviewName;
        const articleImageAlternativeTitle = articleImageAlternativeTitleInput.value;
        const articleContent = CKEDITOR.instances["articleDescription"].getData();
        const articleRecommended = articleRecommendedSwitch.checked;
        const articleDate = new Date().toJSON();

        const data = {
            title: articleTitle,
            content: articleContent,
            date: articleDate,
            imageTitle: articleImageAlternativeTitle,
            imageUrl: `/images/${articleImageName}`,
            isRecommended: articleRecommended
        }

        if (!articleImageName || !articleContent || !articleTitle) {
            this.toastController.renderToastElement("Dodawanie artykułu nie powiodło się. Sprawdź czy podałeś/aś wymagane dane.")
            return;
        }

        if (this.currentAction === 'AddArticle') {
            await addArticle(data);
        } else {
            const currentArticleElement = this.elements.articleElement;
            const currentArticleId = currentArticleElement.dataset.articleId;

            await updateArticle(currentArticleId, data);
        }

        if (!!articleImage) {
            await this.addLocalArticleImage(articleImage, articleImageName);
        }

        window.location.pathname = '/Article/Articles';
    }

    deleteArticle = async () => {
        const currentArticleElement = this.elements.articleElement;
        const currentArticleId = currentArticleElement.dataset.articleId;

        await deleteArticle(currentArticleId);

        window.location.pathname = '/Article/Articles';
    }

    addLocalArticleImage = async (file, fileName) => {
        const formData = new FormData();

        formData.append('formFile', file);
        formData.append('fileName', fileName);

        await addFile("photos", formData);
    }

    generateArticleImageName = (articleImageName) => {
        const idToUrl = idGenerator(4);
        const articleImageUrl = `${articleImageName.replace('.', `_${idToUrl}.`)}`;

        return articleImageUrl;
    }
}