﻿import { updateArticleContent } from '../Model/Article.js'

export class ArticleListController {
    sel = {
        articleListItem: '.js-articleListItem',
        articleIsRecommendedSwitch: '.js-articleIsRecommendedSwitch'
    }

    init = () => {
        this.handleArticleListItems();
    }

    handleArticleListItems = () => {
        const articleListItems = document.querySelectorAll(this.sel.articleListItem);

        articleListItems.forEach(articleListItem => {
            const isRecommendedSwitch = articleListItem.querySelector(this.sel.articleIsRecommendedSwitch);
            const articleId = articleListItem.dataset.articleId;

            isRecommendedSwitch.addEventListener('change', () => this.changeArticleRecommendation(articleId, isRecommendedSwitch)) 
        })
    }

    changeArticleRecommendation = async (articleId, isRecommendedSwitch) => {
        const switchIsChecked = isRecommendedSwitch.checked;
        const data = {
            isRecommended: switchIsChecked
        }

        updateArticleContent(articleId, data);
    }
}