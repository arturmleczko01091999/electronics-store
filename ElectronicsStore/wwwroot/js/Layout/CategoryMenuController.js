﻿export class CategoryMenuController {
    sel = {
        overlap: '.backdrop',
        categoryMenu: '.category-menu',
        categoryMenuBtn: '.category-link-container',
        categoryMenuBtnIcon: '.category-icon',
        categoryMenuItem: '.category-menu-item'
    }

    elements = {
        overlap: document.querySelector(this.sel.overlap),
        categoryMenu: document.querySelector(this.sel.categoryMenu),
        categoryMenuBtn: document.querySelector(this.sel.categoryMenuBtn),
        categoryMenuBtnIcon: document.querySelector(this.sel.categoryMenuBtnIcon),
        categoryMenuItem: document.querySelector(this.sel.categoryMenuItem)
    }

    menuElements = [this.elements.overlap, this.elements.categoryMenu, this.elements.categoryMenuBtn];

    init = () => {
        this.elements.categoryMenuBtn.addEventListener('click', this.showCategoryMenu);
    }

    showCategoryMenu = () => {
        this.menuElements.forEach(menuElement => {
            menuElement.classList.toggle('active');
        })

        document.addEventListener('click', (event) => this.hideCategoryMenu(event));
    }

    hideCategoryMenu = (event) => {
        const target = event.target;
        const targetClassName = target.className;

        if (!targetClassName.includes('category')) {
            this.menuElements.forEach(menuElement => {
                menuElement.classList.remove('active');
            })
        }
    }
}



