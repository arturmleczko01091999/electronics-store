﻿import { deleteProduct } from '../Model/Product.js'

export class EditProductController {
    sel = {
        productElement: '.js-productElement',
        deleteProductButton: '.js-deleteProductButton'
    }

    elements = {
        productElement: document.querySelector(this.sel.productElement),
        deleteProductButton: document.querySelector(this.sel.deleteProductButton)
    }

    init = () => {
        this.handleDeleteProductButton();
    }

    handleDeleteProductButton = () => {
        this.elements.deleteProductButton.addEventListener('click', this.deleteProduct)
    }

    deleteProduct = async () => {
        const productId = this.elements.productElement.dataset.productId;
        await deleteProduct(productId);

        const origin = window.location.origin;
        const urlToProducts = `${origin}/Assortment/Products`;

        window.location.replace(urlToProducts);
    }
}