﻿export class ProductScrollspyController {
    sel = {
        scrollspyItem: '.js-scrollspyItem',
        scrollspyItemLink: '.js-scrollspyItemLink'
    }

    elements = {
        scrollspyItems: document.querySelectorAll(this.sel.scrollspyItem),
        scrollspyItemLinks: document.querySelectorAll(this.sel.scrollspyItemLink)
    }

    init = () => {
        this.handleScrollspyItemLinks();
    }

    handleScrollspyItemLinks = () => {
        this.elements.scrollspyItemLinks.forEach(scrollspyItemLink => {
            scrollspyItemLink.addEventListener('click', () => this.activeScrollspyItemLink(scrollspyItemLink))
        })
    }

    activeScrollspyItemLink = (scrollspyItemLink) => {
        this.elements.scrollspyItems.forEach(scrollspyItem => {
            scrollspyItem.classList.remove('active');
        })

        const scrollspyItemLinkParent = scrollspyItemLink.parentElement;
        scrollspyItemLinkParent.classList.add('active');
    }
}