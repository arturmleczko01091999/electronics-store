﻿export class ProductCharacteristicsController {
    sel = {
        productForm: '.js-productForm',
        characteristicsInput: '.js-characteristicsInput',
        characteristicsTable: '.js-characteristicsTable',
        characteristicsTableBody: '.js-characteristicsTableBody',
        characteristicRow: '.js-characteristicRow',
        characteristicName: '.js-characteristicName',
        characteristicValue: '.js-characteristicValue',
        addCharacteristicButton: '.js-addCharacteristicButton',
        deleteCharacteristicsRowButton: '.js-deleteCharacteristicsRowButton'
    }

    elements = {
        productForm: document.querySelector(this.sel.productForm),
        characteristicsInput: document.querySelector(this.sel.characteristicsInput),
        characteristicsTable: document.querySelector(this.sel.characteristicsTable),
        characteristicsTableBody: document.querySelector(this.sel.characteristicsTableBody),
        addCharacteristicButton: document.querySelector(this.sel.addCharacteristicButton),
        characteristicRows: NodeList
    }

    init = () => {
        this.handleAddCharacteristicButton();
        this.handleCharacteristicRows();
        this.handleProductForm();
    }

    handleAddCharacteristicButton = () => {
        this.elements.addCharacteristicButton.addEventListener('click', this.addCharacteristicRow)
    }

    handleCharacteristicRows = () => {
        this.elements.characteristicRows = document.querySelectorAll(this.sel.characteristicRow);

        this.elements.characteristicRows.forEach(characteristicRow => {
            const deleteButton = characteristicRow.querySelector(this.sel.deleteCharacteristicsRowButton);

            deleteButton.addEventListener('click', () => this.deleteCharacteristicRow(characteristicRow));
        })
    }

    handleProductForm = () => {
        this.elements.productForm.addEventListener('submit', this.saveCharacteristics);
    }

    addCharacteristicRow = () => {
        const characteristicRow = `
            <tr class="js-characteristicRow">
                <td>
                    <input type="text" class="form-control form-control--product-lg js-characteristicName" placeholder="Wpisz nazwę cechy" />
                <td class="characteristic-value-wrapper">
                    <input type="text" class="form-control form-control--product js-characteristicValue" placeholder="Wpisz wartość cechy" />
                    <img src="/icons/trash.svg" alt="Kosz" class="trash-icon js-deleteCharacteristicsRowButton" />
                </td>
            </tr>
        `;

        this.elements.characteristicsTableBody.insertAdjacentHTML('beforeend', characteristicRow);
        this.handleCharacteristicRows();
        this.toggleEmptyClass();
    }

    deleteCharacteristicRow = (characteristicRow) => {
        characteristicRow.remove();
        this.toggleEmptyClass();
    }

    toggleEmptyClass = () => {
        this.elements.characteristicRows = document.querySelectorAll(this.sel.characteristicRow);

        if (this.elements.characteristicRows.length === 0) {
            this.elements.characteristicsTable.classList.add('empty');
        } else {
            this.elements.characteristicsTable.classList.remove('empty');
        }
    }

    saveCharacteristics = () => {
        const characteristicsData = [];

        this.elements.characteristicRows.forEach(characteristicRow => {
            const characteristicName = characteristicRow.querySelector(this.sel.characteristicName);
            const characteristicValue = characteristicRow.querySelector(this.sel.characteristicValue);

            const name = characteristicName.value;
            const value = characteristicValue.value;

            const characteristicData = {
                Name: name,
                Value: value
            }

            if (name !== '' || value !== '') {
                characteristicsData.push(characteristicData);
                this.setCharacteristicsData(characteristicsData);
            }
        })
    }

    setCharacteristicsData = (characteristicsData) => {
        const characteristicsDataAsJSON = JSON.stringify(characteristicsData);
        this.elements.characteristicsInput.value = characteristicsDataAsJSON;
    }
}