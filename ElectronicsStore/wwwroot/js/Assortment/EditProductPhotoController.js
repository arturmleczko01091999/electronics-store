﻿import { deleteProductPhoto, updateProductPhoto } from '../Model/ProductPhoto.js';
import { addFile } from '../Model/File.js';
import { idGenerator } from '../Utils/idGenerator.js'

export class EditProductPhotoController {
    sel = {
        productForm: '.js-productForm',
        productPhotoPreviewWrapper: '.js-productPhotoPreviewWrapper',
        deleteProductPhoto: '.js-deleteProductPhoto',
        editProductPhoto: '.js-editProductPhoto',
        photoUploaderInput: '.js-photoUploaderInput',
        addPhotoUploaderInput: '.js-addPhotoUploaderInput',
        currentImageName: '.js-currentImageName',
        currentAddImageName: '.js-currentAddImageName',
        photoAlternativeTitle: '.js-photoAlternativeTitle',
        photoIsMainSwitch: '.js-photoIsMainSwitch',
        addProductPhotoButton: '.js-addProductPhotoButton',
        saveProductPhotoButton: '.js-saveProductPhotoButton',
        productPhotoPreview: '.js-productPhotoPreview'
    }

    elements = {
        productForm: document.querySelector(this.sel.productForm),
        productPhotoPreviewWrappers: document.querySelectorAll(this.sel.productPhotoPreviewWrapper),
        addPhotoUploaderInput: document.querySelector(this.sel.addPhotoUploaderInput),
        currentAddImageName: document.querySelector(this.sel.currentAddImageName),
        addProductPhotoButton: document.querySelector(this.sel.addProductPhotoButton)
    }

    init = () => {
        this.handleAddPhotoUploader();
        this.handleAddProductPhotoButton();
        this.handleProductPhotoPreviewWrappers();
        this.lockSwitches();
        this.handleSwitches();
    }

    handlePhotoUploader = (productPhotoId) => {
        const productPhotoModal = document.querySelector(`[data-product-photo-modal-id="${productPhotoId}"]`);
        const photoUploaderInput = productPhotoModal.querySelector(this.sel.photoUploaderInput);
        const currentImageName = productPhotoModal.querySelector(this.sel.currentImageName);

        photoUploaderInput.addEventListener('change', () => {
            this.setImageName(photoUploaderInput, currentImageName);
        })
    }

    handleAddPhotoUploader = () => {
        const { addPhotoUploaderInput, currentAddImageName } = this.elements;
        this.elements.addPhotoUploaderInput.addEventListener('change', () => {
            this.setImageName(addPhotoUploaderInput, currentAddImageName)
        });
    }

    handleAddProductPhotoButton = () => {
        this.elements.addProductPhotoButton.addEventListener('click', this.addProductPhoto);
    }

    handleSwitches = () => {
        const allSwitches = document.querySelectorAll(this.sel.photoIsMainSwitch);

        allSwitches.forEach(singleSwitch => {
            singleSwitch.addEventListener('change', () => {
                this.lockSwitches();
            })
        })
    }

    handleProductPhotoPreviewWrappers = () => {
        this.elements.productPhotoPreviewWrappers.forEach(productPhotoPreviewWrapper => {
            const productPhotoId = productPhotoPreviewWrapper.dataset.productPhotoId;
            const deleteProductPhoto = productPhotoPreviewWrapper.querySelector(this.sel.deleteProductPhoto);
            const editProductPhoto = productPhotoPreviewWrapper.querySelector(this.sel.editProductPhoto);

            deleteProductPhoto.addEventListener('click', () => {
                this.deleteProductPhoto(productPhotoPreviewWrapper, productPhotoId);
            })

            this.editProductPhoto(productPhotoPreviewWrapper, productPhotoId);
            this.handlePhotoUploader(productPhotoId);
        })
    }

    deleteProductPhoto = async (productPhotoPreviewWrapper, productPhotoId) => {
        const productPhotoModal = document.querySelector(`[data-product-photo-modal-id="${productPhotoId}"]`);

        productPhotoPreviewWrapper.remove();
        productPhotoModal.remove();

        await deleteProductPhoto(productPhotoId)
        this.lockSwitches();
    }

    editProductPhoto = (productPhotoPreviewWrapper, productPhotoId) => {
        const productPhotoModal = document.querySelector(`[data-product-photo-modal-id="${productPhotoId}"]`);
        const saveProductPhotoButton = productPhotoModal.querySelector(this.sel.saveProductPhotoButton);

        saveProductPhotoButton.addEventListener('click', () => this.saveProductPhoto(productPhotoModal, productPhotoId));
    }

    addProductPhoto = () => {
        this.elements.productForm.submit();
    }

    setImageName = (photoUploadInput, currentImageName) => {
        const [file] = photoUploadInput.files;

        if (file) {
            currentImageName.textContent = file.name;
        }
    }

    saveProductPhoto = async (productPhotoModal, productPhotoId) => {
        const photoUploaderInput = productPhotoModal.querySelector(this.sel.photoUploaderInput);
        const currentImageNameElement = productPhotoModal.querySelector(this.sel.currentImageName);
        const photoAlternativeTitleElement = productPhotoModal.querySelector(this.sel.photoAlternativeTitle);
        const photoIsMainSwitchElement = productPhotoModal.querySelector(this.sel.photoIsMainSwitch);
        const productPhotoPreview = document.querySelector(`[data-product-photo-id="${productPhotoId}"] ${this.sel.productPhotoPreview}`);

        const currentImageName = currentImageNameElement.textContent;
        const photoAlternativeTitle = photoAlternativeTitleElement.value;
        const photoIsMainSwitch = photoIsMainSwitchElement.checked;
        const productPhotoPreviewName = productPhotoPreview.src.split("/").pop();

        const productPhotoName = productPhotoPreviewName === currentImageName ? productPhotoPreviewName : this.generateProductPhotoName(currentImageName);

        const dataToChange = {
            url: `/images/${productPhotoName}`,
            title: photoAlternativeTitle,
            isMain: photoIsMainSwitch
        }

        const dataToChangeAsJSON = JSON.stringify(dataToChange);

        await updateProductPhoto(productPhotoId, dataToChangeAsJSON);

        const [file] = photoUploaderInput.files;

        if (file) {
            this.changeProductPreviewImage(productPhotoId, file);
            this.addProductPhotoFile(file, productPhotoName);
        }

        this.lockSwitches();
    }

    generateProductPhotoName = (productPhotoName) => {
        const idToUrl = idGenerator(4);
        const productPhotoUrl = `${productPhotoName.replace('.', `_${idToUrl}.`)}`;

        return productPhotoUrl;
    }

    changeProductPreviewImage = (productPhotoId, file) => {
        const productPhotoPreview = document.querySelector(`[data-product-photo-id="${productPhotoId}"] ${this.sel.productPhotoPreview}`);

        const photoFile = URL.createObjectURL(file);
        productPhotoPreview.src = photoFile;
    }

    addProductPhotoFile = async (file, fileName) => {
        const formData = new FormData();

        formData.append('formFile', file);
        formData.append('fileName', fileName);

        await addFile("photos", formData);
    }

    lockSwitches = () => {
        const allSwitches = document.querySelectorAll(this.sel.photoIsMainSwitch);
        const notMainSwitches = [...allSwitches].filter(singleSwitch => singleSwitch.checked === false);

        if (allSwitches.length === notMainSwitches.length) {
            allSwitches.forEach(singleSwitch => {
                if (singleSwitch.hasAttribute('disabled')) {
                    singleSwitch.removeAttribute('disabled')
                }
            })
        } else {
            notMainSwitches.forEach(singleSwitch => {
                singleSwitch.setAttribute('disabled', 'disabled')
            })
        }
    }
}