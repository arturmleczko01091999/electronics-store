﻿export class ProductBasicsController {
    sel = {
        productForm: '.js-productForm',
        productNameInput: '.js-productNameInput',
        numberCharactersToUseProductName: '.js-numberCharactersToUseProductName',
        backgroundInput: '.js-backgroundInput',
        backgroundRow: '.js-backgroundRow',
        backgroundName: '.js-backgroundName',
        backgroundValue: '.js-backgroundValue',
        oldPriceField: '.js-ProductOldPrice',
        oldPriceFieldInput: '.js-ProductOldPriceInput',
        priceField: '.js-ProductPrice',
        specialOfferInput: '.js-specialOfferInput',
        promotionOfferInput: '.js-promotionOfferInput'
    }

    element = {
        productForm: document.querySelector(this.sel.productForm),
        productNameInput: document.querySelector(this.sel.productNameInput),
        numberCharactersToUseProductName: document.querySelector(this.sel.numberCharactersToUseProductName),
        backgroundInput: document.querySelector(this.sel.backgroundInput),
        backgroundRows: document.querySelectorAll(this.sel.backgroundRow),
        oldPriceField: document.querySelector(this.sel.oldPriceField),
        oldPriceFieldInput: document.querySelector(this.sel.oldPriceFieldInput),
        priceField: document.querySelector(this.sel.priceField),
        specialOfferInputs: document.querySelectorAll(this.sel.specialOfferInput),
    }

    init = () => {
        this.handleProductNameInput();
        this.setNumberCharactersToUseProductName(this.element.productNameInput);
        this.handleProductForm();
        this.toggleOldPriceVisibility();
        this.handleSpecialOfferInputs();
    }

    handleProductNameInput = () => {
        this.element.productNameInput.addEventListener('input', (e) => {
            const inputElement = e.target;

            this.setNumberCharactersToUseProductName(inputElement);
        })
    }

    handleSpecialOfferInputs = () => {
        this.element.specialOfferInputs.forEach(specialOfferInput => {
            specialOfferInput.addEventListener("change", this.toggleOldPriceVisibility);
        })
    }

    handleProductForm = () => {
        this.element.productForm.addEventListener('submit', this.handleBackgroundData);
    }

    handleBackgroundData = () => {
        const backgroundData = [];

        this.element.backgroundRows.forEach(backgroundRow => {

            const backgroundName = backgroundRow.querySelector(this.sel.backgroundName);
            const backgroundValue = backgroundRow.querySelector(this.sel.backgroundValue);

            const name = backgroundName.textContent;
            const value = backgroundValue.value;

            const background = {
                Name: `${backgroundName.textContent}`,
                Value: `${backgroundValue.value}`
            }

            backgroundData.push(background);
        });

        const backgroundDataAsJSON = JSON.stringify(backgroundData);

        this.element.backgroundInput.value = backgroundDataAsJSON;
    }

    setNumberCharactersToUseProductName = (inputElement) => {
        const maxProductNameLength = inputElement.maxLength;
        const productNameInputLength = inputElement.value.length;

        const numberCharactersToUseProductName = maxProductNameLength - productNameInputLength;
        const kindOfLeft = numberCharactersToUseProductName === 1 ? "Pozostał" : "Pozostało";
        const kindOfCharacters = numberCharactersToUseProductName === 1 ? "znak" : "znaków";

        this.element.numberCharactersToUseProductName.innerText = `${kindOfLeft} ${numberCharactersToUseProductName} ${kindOfCharacters}`;
    }

    toggleOldPriceVisibility = () => {
        const promotionOfferInput = document.querySelector(this.sel.promotionOfferInput);
        const promotionOfferIsChecked = promotionOfferInput.checked;


        if (promotionOfferIsChecked) {
            const oldPrice = this.element.oldPriceFieldInput.value;

            this.element.oldPriceFieldInput.value = oldPrice !== '' ? oldPrice : '0.00';
            this.element.oldPriceField.classList.replace('d-none', 'd-flex');
            this.element.priceField.classList.add('pt-4')
        } else {
            this.element.oldPriceFieldInput.value = '';
            this.element.oldPriceField.classList.replace('d-flex', 'd-none');
            this.element.priceField.classList.remove('pt-4');
        }
    }
}