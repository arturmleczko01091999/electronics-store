﻿export class AddProductPhotoController {
    sel = {
        photoUploaderInput: '.js-photoUploaderInput',
        addProductPhotoButton: '.js-addProductPhotoButton',
        addProductPhotoButtonText: '.js-addProductPhotoButtonText',
        addedProductPhotoPreview: '.js-addedProductPhotoPreview',
        addedProductPhotoPreviewWrapper: '.js-addedProductPhotoPreviewWrapper',
        deleteProductPhoto: '.js-deleteProductPhoto',
        currentImageName: '.js-currentImageName',
        openModelButton: '.js-openModelButton',
        productPhotoModalTitle: '.js-productPhotoModalTitle',
        photoAlternativeTitle: '.js-photoAlternativeTitle',
        photoIsMainSwitch: '.js-photoIsMainSwitch'
    }

    elements = {
        photoUploaderInput: document.querySelector(this.sel.photoUploaderInput),
        addProductPhotoButton: document.querySelector(this.sel.addProductPhotoButton),
        addProductPhotoButtonText: document.querySelector(this.sel.addProductPhotoButtonText),
        addedProductPhotoPreview: document.querySelector(this.sel.addedProductPhotoPreview),
        addedProductPhotoPreviewWrapper: document.querySelector(this.sel.addedProductPhotoPreviewWrapper),
        deleteProductPhoto: document.querySelector(this.sel.deleteProductPhoto),
        currentImageName: document.querySelector(this.sel.currentImageName),
        openModelButton: document.querySelector(this.sel.openModelButton),
        productPhotoModalTitle: document.querySelector(this.sel.productPhotoModalTitle),
        photoAlternativeTitle: document.querySelector(this.sel.photoAlternativeTitle),
        photoIsMainSwitch: document.querySelector(this.sel.photoIsMainSwitch)
    }

    init = () => {
        this.handlePhotoUploader();
        this.handleAddProductPhotoButton();
    }

    handleAddProductPhotoButton = () => {
        this.elements.addProductPhotoButton.addEventListener('click', this.previewProductPhoto);
    }

    handlePhotoUploader = () => {
        this.elements.photoUploaderInput.addEventListener('change', this.setImageName);
    }

    handleDetleProductPhotoButton = () => {
        this.elements.deleteProductPhoto.addEventListener('click', this.deleteAddedProductPhoto);
    }

    previewProductPhoto = () => {
        const [file] = this.elements.photoUploaderInput.files;

        if (file) {
            const imageSrc = URL.createObjectURL(file);
            this.elements.addedProductPhotoPreview.src = imageSrc;
            this.elements.addedProductPhotoPreviewWrapper.classList.remove('d-none');
            this.elements.openModelButton.classList.add('d-none');
            this.changeModalType('edit');

            this.handleDetleProductPhotoButton();
        }
    }

    setImageName = () => {
        const [file] = this.elements.photoUploaderInput.files;

        if (file) {
            this.elements.currentImageName.textContent = file.name;
        }
    }

    changeModalType = (modalType) => {
        if (modalType === 'edit') {
            this.elements.productPhotoModalTitle.textContent = "Edytuj zdjęcie";
            this.elements.addProductPhotoButtonText.textContent = "Zapisz";
        } else if (modalType === 'add') {
            this.elements.productPhotoModalTitle.textContent = "Dodaj zdjęcie";
            this.elements.addProductPhotoButtonText.textContent = "Dodaj";
        }
    }

    deleteAddedProductPhoto = () => {
        this.elements.currentImageName.textContent = '';
        this.elements.photoAlternativeTitle.value = '';
        this.elements.photoIsMainSwitch.checked = false;

        this.elements.addedProductPhotoPreviewWrapper.classList.add('d-none');
        this.elements.openModelButton.classList.remove('d-none');

        this.changeModalType('add');
    }
}