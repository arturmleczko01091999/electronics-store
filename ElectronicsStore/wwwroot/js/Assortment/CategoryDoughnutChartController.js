﻿import { getCategoryDoughnutChart} from '../Model/Chart.js';

export class CategoryDoughnutChartController {
    sel = {
        chart: 'categoryDoughnutChart',
        noCategoryDoughnutChartInfo: '.js-noCategoryDoughnutChartInfo'
    }

    element = {
        chart: document.getElementById(this.sel.chart),
        noCategoryDoughnutChartInfo: document.querySelector(this.sel.noCategoryDoughnutChartInfo)
    }

    chart = {
        data: {},
        config: {}
    }

    init = () => {
        this.getChartData()
            .then(() => this.getChartConfig()
            .then(() => this.generateChart()));
    }

    getChartData = async () => {
        this.chart.data = await getCategoryDoughnutChart();
    }

    getChartConfig = async () => {
        const config = {
            type: 'doughnut',
            data: {
                labels: this.chart.data.labels,
                datasets: [{
                    data: this.chart.data.data,
                    backgroundColor: [
                        'rgb(249, 65, 68)',
                        'rgb(243, 114, 44)',
                        'rgb(248, 150, 30)',
                        'rgb(249, 199, 79)',
                        'rgb(144, 190, 109)',
                        'rgb(67, 170, 139)',
                        'rgb(25, 130, 196)',
                        'rgb(46, 196, 182)',
                        'rgb(13, 59, 102)',
                        'rgb(250, 240, 202)',
                        'rgb(244, 211, 94)',
                        'rgb(238, 150, 75)',
                        'rgb(249, 87, 56)',
                        'rgb(1, 22, 39)',
                        'rgb(255, 146, 177)'
                    ],
                    hoverOffset: 4
                }]
            },
            options: {
                plugins: {
                    legend: {
                        display: false
                    },
                    tooltip: {
                        boxPadding: 3
                    }
                }
            }
        };

        this.chart.config = config;
    }

    generateChart = () => {
        if (!this.chart.data.lables && !this.chart.data.data[0]) {
            this.element.chart.classList.add('d-none');
            this.element.noCategoryDoughnutChartInfo.classList.remove('d-none');

            return;
        }

        const chart = new Chart(
            this.element.chart,
            this.chart.config
        );
    }
}


