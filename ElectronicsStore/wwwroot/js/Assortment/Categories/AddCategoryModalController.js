﻿import { idGenerator } from '../../Utils/idGenerator.js';
import { addCategory } from '../../Model/Category.js';
import { addFile } from '../../Model/File.js';
import { ToasterController } from '../../ToasterController.js';

export class AddCategoryModalController {
    sel = {
        addCategoryButton: '.js-addCategoryButton',
        categoryNameInput: '.js-categoryNameInput',
        categoryImageUploaderInput: '.js-categoryImageUploaderInput',
        categoryImageAlternativeTitleInput: '.js-categoryImageAlternativeTitleInput',
        categoryTableBody: '.js-categoryTableBody',
        currentCategoryImageName: '.js-currentCategoryImageName'
    }

    elements = {
        addCategoryButton: document.querySelector(this.sel.addCategoryButton),
        categoryNameInput: document.querySelector(this.sel.categoryNameInput),
        categoryImageUploaderInput: document.querySelector(this.sel.categoryImageUploaderInput),
        categoryImageAlternativeTitleInput: document.querySelector(this.sel.categoryImageAlternativeTitleInput),
        categoryTableBody: document.querySelector(this.sel.categoryTableBody),
        currentCategoryImageName: document.querySelector(this.sel.currentCategoryImageName)
    }

    toastController = new ToasterController();

    init = () => {
        this.handleCategoryImageUploader();
        this.handleAddCategoryButton();
    }

    handleCategoryImageUploader = () => {
        this.elements.categoryImageUploaderInput.addEventListener('change', () => {
            this.setImageName(this.elements.categoryImageUploaderInput, this.elements.currentCategoryImageName);
        })
    }

    handleAddCategoryButton = () => {
        this.elements.addCategoryButton.addEventListener('click', this.addCategory)
    }

    addCategory = async () => {
        const { categoryNameInput, categoryImageUploaderInput, categoryImageAlternativeTitleInput } = this.elements;

        const categoryName = categoryNameInput.value;
        const categoryImage = categoryImageUploader.files[0];
        const categoryImageName = !!categoryImage ? this.generateCategoryImageName(categoryImage.name) : null;
        const categoryImageAlternativeTitle = categoryImageAlternativeTitleInput.value;


        const categoryData = {
            name: categoryName,
            imageUrl: `/images/${categoryImageName}`,
            imageTitle: categoryImageAlternativeTitle
        }

        if (!!categoryName && !!categoryImageName) {
            await addCategory(categoryData);
            await this.addLocalCategoryImage(categoryImage, categoryImageName);

            window.location.reload();
        } else {
            const headingErrorMessage = "Dodawanie kategorii nie powiodło się.";
            if (!categoryName && !!categoryImageName) {
                this.toastController.renderToastElement(`${headingErrorMessage} Podaj nazwę kategorii.`)
            } else if (!categoryImageName && !!categoryName) {
                this.toastController.renderToastElement(`${headingErrorMessage} Dodaj zdjęcie kategorii.`)
            } else {
                this.toastController.renderToastElement(`${headingErrorMessage} Sprawdź czy podałeś/aś wszytskie dane.`)
            }

            return;
        }
    }

    generateCategoryImageName = (categoryImageName) => {
        const idToUrl = idGenerator(4);
        const categoryImageUrl = `${categoryImageName.replace('.', `_${idToUrl}.`)}`;

        return categoryImageUrl;
    }

    addLocalCategoryImage = async (file, fileName) => {
        const formData = new FormData();

        formData.append('formFile', file);
        formData.append('fileName', fileName);

        await addFile("photos", formData);
    }

    setImageName = (imageUploadInput, currentImageName) => {
        const [file] = imageUploadInput.files;

        if (file) {
            currentImageName.textContent = file.name;
        }
    }
}