﻿import { EditCategoryTabController } from './EditCategoryTabsController.js';

export class CategoriesController {
    editCategoryTab;

    sel = {
        categoryRow: '.js-categoryRow',
        categoryEditTab: 'data-category-edit-tab-id'
    }

    elements = {
        categoryRows: document.querySelectorAll(this.sel.categoryRow)
    }

    init = () => {
        this.editCategoryTab = new EditCategoryTabController();
        this.handleCategoryRows();
    }

    handleCategoryRows = () => {
        this.elements.categoryRows.forEach(categoryRow => {
            const categoryId = categoryRow.dataset.categoryId;
            categoryRow.addEventListener('click', () => {
                this.openEdiCategoryTab(categoryId);
                this.editCategoryTab.handleActiveCategoryEditTab(categoryId);
            })
        })
    }

    openEdiCategoryTab = (categoryId) => {
        const categoryEtitTabs = document.querySelectorAll(`[${this.sel.categoryEditTab}]`);

        categoryEtitTabs.forEach(categoryEditTab => {
            const categoryEditTabId = categoryEditTab.dataset.categoryEditTabId;

            if (categoryEditTabId === categoryId) {
                if (categoryEditTab.className.includes('d-none')) {
                    categoryEditTab.classList.remove('d-none');
                }
            } else {
                if (!categoryEditTab.className.includes('d-none')) {
                    categoryEditTab.classList.add('d-none');
                }
            }
        })
    }
}