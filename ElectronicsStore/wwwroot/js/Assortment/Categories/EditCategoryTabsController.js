﻿import { updateCategory, deleteCategory } from '../../Model/Category.js'
import { idGenerator } from '../../Utils/idGenerator.js';
import { addFile } from '../../Model/File.js';
import { ToasterController } from '../../ToasterController.js';

export class EditCategoryTabController {
    sel = { 
        categoryRow: 'data-category-id',
        categoryEditTab: 'data-category-edit-tab-id',
        saveCategoryButton: '.js-saveCategoryButton',
        deleteCategoryButton: '.js-deleteCategoryButton',
        categoryImageUploaderInput: '.js-categoryImageUploaderInput',
        categoryImagePreview: '.js-categoryImagePreview',
        categoryNameInput: '.js-categoryNameInput',
        categoryImageAlternativeTitleInput: '.js-categoryImageAlternativeTitleInput'
    }

    toastController = new ToasterController();

    handleActiveCategoryEditTab = (categoryId) => {
        const activeCategoryEditTab = document.querySelector(`[${this.sel.categoryEditTab}="${categoryId}"]`);

        const saveCategoryButton = activeCategoryEditTab.querySelector(this.sel.saveCategoryButton);
        const deleteCategoryButton = activeCategoryEditTab.querySelector(this.sel.deleteCategoryButton);

        const categoryImageUploaderInput = activeCategoryEditTab.querySelector(this.sel.categoryImageUploaderInput);
        const categoryImagePreview = activeCategoryEditTab.querySelector(this.sel.categoryImagePreview);

        saveCategoryButton.addEventListener('click', () => {
            this.saveCategory(categoryId, activeCategoryEditTab);  
        })

        deleteCategoryButton.addEventListener('click', () => {
            this.deleteCategory(categoryId);
        })

        categoryImageUploaderInput.addEventListener('change', () => {
            this.changeCategoryImagePreview(categoryImageUploaderInput, categoryImagePreview);
        })
    }

    saveCategory = async (categoryId, activeCategoryEditTab) => {
        const categoryImageUploaderInput = activeCategoryEditTab.querySelector(this.sel.categoryImageUploaderInput);
        const categoryNameInput = activeCategoryEditTab.querySelector(this.sel.categoryNameInput);
        const categoryImageAlternativeTitleInput = activeCategoryEditTab.querySelector(this.sel.categoryImageAlternativeTitleInput);
        const categoryImagePreview = activeCategoryEditTab.querySelector(this.sel.categoryImagePreview);

        const categoryName = categoryNameInput.value;
        const categoryImage = categoryImageUploaderInput.files[0];
        const categoryImagePreviewName = categoryImagePreview.src.split("/").pop();
        const categoryImageName = !!categoryImage ? this.generateCategoryImageName(categoryImage.name) : categoryImagePreviewName;
        const categoryImageAlternativeTitle = categoryImageAlternativeTitleInput.value;

        const categoryData = {
            name: categoryName,
            imageUrl: `/images/${categoryImageName}`,
            imageTitle: categoryImageAlternativeTitle
        }

        if (!!categoryName && !!categoryImageName) {
            await updateCategory(categoryId, categoryData);
        } else {
            const headingErrorMessage = "Dodawanie kategorii nie powiodło się.";
            if (!categoryName && !!categoryImageName) {
                this.toastController.renderToastElement(`${headingErrorMessage} Podaj nazwę kategorii.`)
            } else if (!categoryImageName && !!categoryName) {
                this.toastController.renderToastElement(`${headingErrorMessage} Dodaj zdjęcie kategorii.`)
            } else {
                this.toastController.renderToastElement(`${headingErrorMessage} Sprawdź czy podałeś/aś wszytskie dane.`)
            }
            return;
        }

        if (!!categoryImage) {
            await this.addLocalCategoryImage(categoryImage, categoryImageName);
        }

        window.location.reload();
    }

    deleteCategory = async (categoryId) => {
        await deleteCategory(categoryId);
        window.location.reload();
    }

    changeCategoryImagePreview = (categoryImageUploaderInput, categoryImagePreview) => {
        const [file] = categoryImageUploaderInput.files;
        const image = URL.createObjectURL(file);

        categoryImagePreview.src = image;
    }

    addLocalCategoryImage = async (file, fileName) => {
        const formData = new FormData();

        formData.append('formFile', file);
        formData.append('fileName', fileName);

        await addFile("photos", formData);
    }

    generateCategoryImageName = (categoryImageName) => {
        const idToUrl = idGenerator(4);
        const categoryImageUrl = `${categoryImageName.replace('.', `_${idToUrl}.`)}`;

        return categoryImageUrl;
    }
}