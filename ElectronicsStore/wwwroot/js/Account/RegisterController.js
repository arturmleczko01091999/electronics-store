﻿export class RegisterController {
    sel = {
        passowordInput: '[data-password]',
        confirmPasswordInput: '[data-confirm-password]',
        passwordIcon: '.password-icon'
    }

    element = {
        passowordInput: document.querySelector(this.sel.passowordInput),
        confirmPasswordInput: document.querySelector(this.sel.confirmPasswordInput)
    }

    init = () => {
        this.handlePassowordInputs();
    }

    handlePassowordInputs = () => {
        const passwordInputs = [this.element.passowordInput, this.element.confirmPasswordInput];

        passwordInputs.forEach(passwordInput => {
            this.handlePasswordIcon(passwordInput);
        })
    }

    handlePasswordIcon = (passwordInput) => {
        const passwordIcon = passwordInput.nextElementSibling;
        passwordIcon.addEventListener('click', () => {
            this.togglePasswordVisibility(passwordInput)
        });
    }

    togglePasswordVisibility = (passwordInput) => {
        const type = passwordInput.type;

        if (type === 'password') {
            passwordInput.type = 'text';
        } else {
            passwordInput.type = 'password';
        }
    }
}