﻿export class LoginController {
    sel = {
        passowordInput: '.password-input',
        passwordIcon: '.password-icon'
    }

    element = {
        passowordInput: document.querySelector(this.sel.passowordInput),
        passwordIcon: document.querySelector(this.sel.passwordIcon)
    }

    init = () => {
        this.handlePassowordInput();
    }

    handlePassowordInput = () => {
        this.element.passwordIcon.addEventListener('click', this.togglePasswordVisibility);
    }

    togglePasswordVisibility = () => {
        const type = this.element.passowordInput.type;
      
        if (type === 'password') {
            this.element.passowordInput.type = 'text';
        } else {
            this.element.passowordInput.type = 'password';
        }
    }
}