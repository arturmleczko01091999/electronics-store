﻿export class ProductsController {
    sel = {
        tilesContainer: '.tiles-container',
        tilesViewButton: '.tiles-view-button',
        listViewButton: '.list-view-button'
    };

    elements = {
        tilesContainer: document.querySelector(this.sel.tilesContainer),
        tilesViewButton: document.querySelector(this.sel.tilesViewButton),
        listViewButton: document.querySelector(this.sel.listViewButton)
    };

    sessionStorageKey = 'productsViewKind';

    defaultClass = 'tiles-container';
    tilesViewClass = 'tiles-view';
    listViewClass = 'list-view';

    viewButtons = [
        {
            element: this.elements.tilesViewButton,
            viewClass: this.tilesViewClass
        },
        {
            element: this.elements.listViewButton,
            viewClass: this.listViewClass
        }
    ];

    init = () => {
        this.getViewKindFromSessionStorage();

        this.viewButtons.forEach(viewButton => {
            viewButton.element.addEventListener('click', () => {
                this.acitveViewButton(viewButton.element);
                this.changeContainerView(viewButton.viewClass);
            })
        })
    }

    acitveViewButton = (clickedButton) => {
        this.viewButtons.forEach(viewButton => {
            const button = viewButton.element;

            if (button === clickedButton) {
                button.classList.add('active');
            } else {
                button.classList.remove('active');
            }
        })
    }

    changeContainerView = (viewClass) => {
        this.elements.tilesContainer.className = `${this.defaultClass} ${viewClass}`;
        this.saveViewKindToSessionStorage(viewClass);
    }

    saveViewKindToSessionStorage = (viewKind) => {
        sessionStorage.setItem(this.sessionStorageKey, viewKind);
    }

    getViewKindFromSessionStorage = () => {
        const viewKind = sessionStorage.getItem(this.sessionStorageKey);
        let viewClass;

        if (!viewKind) {
            this.saveViewKindToSessionStorage(this.tilesViewClass);
            viewClass = this.tilesViewClass;
        } else {
            viewClass = viewKind;
        }

        const currentViewButton = this.findCurrentViewButton(viewClass);

        this.acitveViewButton(currentViewButton);
        this.elements.tilesContainer.className = `${this.defaultClass} ${viewClass}`;
    }

    findCurrentViewButton = (viewClass) => {
        const currentViewButton = this.viewButtons.find(viewButton => {
            return viewButton.viewClass === viewClass;
        })

        return currentViewButton.element;
    }
}