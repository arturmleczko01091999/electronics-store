﻿using System.Collections.Generic;
using System.Linq;
using ElectronicsStore.Models;
using ElectronicsStore.Repositories;

namespace TestElectronicsStore.TestRepositories
{
    public class TestCategoryRepository : ICategoryRepository
    {
        private List<Category> _categories;

        public TestCategoryRepository()
        {
            _categories = new List<Category>()
            {
                new Category()
                {
                    CategoryId = 1,
                    Name = "Testowa kategoria 1",
                    ImageUrl = "/images/testowa_grafka_1.jpg",
                    ImageTitle = "Testowy tekst alternatywny grafiki 1",
                    Products = new List<Product>()
                    {
                        new Product()
                        {
                            ProductId = 1,
                            Name = "Testowy produkt 1"
                        }
                    }
                },
                new Category()
                {
                    CategoryId = 2,
                    Name = "Testowa kategoria 2",
                    ImageUrl = "/images/testowa_grafka_2.jpg",
                    ImageTitle = "Testowy tekst alternatywny grafiki 2",
                    Products = new List<Product>()
                    {
                        new Product()
                        {
                            ProductId = 1,
                            Name = "Testowy produkt 1"
                        }
                    }
                }
            };
        }

        public void AddCategory(Category category)
        {
            _categories.Add(category);
        }

        public void UpdateCategory(Category category)
        {
            Category updatedCategory = _categories
                .Where(category => category.CategoryId == category.CategoryId)
                .FirstOrDefault();

            if (updatedCategory != null)
            {
                updatedCategory = category;
            }
        }

        public void DeleteCategory(Category category)
        {
            _categories.Remove(category);
        }

        public Category GetCategory(int id)
        {
            return _categories
                .Where(category => category.CategoryId == id)
                .FirstOrDefault();
        }

        public Category GetCurrentCategory(int? id)
        {
            return _categories
                .Where(category => category.CategoryId == id)
                .FirstOrDefault();
        }

        public Category GetLastCategory()
        {
            return _categories
                .OrderBy(category => category.CategoryId)
                .LastOrDefault();
        }

        public List<Category> GetAllCategories()
        {
            return _categories
                .OrderBy(category => category.CategoryId)
                .ToList();
        }
    }
}
