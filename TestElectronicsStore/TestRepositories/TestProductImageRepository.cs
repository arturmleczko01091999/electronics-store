﻿using System;
using System.Collections.Generic;
using System.Linq;
using ElectronicsStore.Models;
using ElectronicsStore.Repositories;

namespace TestElectronicsStore.TestRepositories
{
    public class TestProductImageRepository : IProductImageRepository
    {
        private List<ProductImage> _productImages;

        public TestProductImageRepository()
        {
            _productImages = new List<ProductImage>()
            {
                new ProductImage()
                {
                    ProductImageId = 1,
                    Title = "Testowy tytuł 1",
                    Url = "/images/testowa_grafka_1.jpg",
                    IsMain = true,
                    ProductId = 1,
                    Product = new Product()
                },
                new ProductImage()
                {
                    ProductImageId = 2,
                    Title = "Testowy tytuł 2",
                    Url = "/images/testowa_grafka_2.jpg",
                    IsMain = false,
                    ProductId = 2,
                    Product = new Product()
                },
            };
        }

        public void AddProductImage(ProductImage productImage)
        {
            _productImages.Add(productImage);
        }

        public void UpdateProductImage(ProductImage productImage)
        {
            ProductImage updatedProductImage = _productImages
                .Where(productImage => productImage.ProductImageId == productImage.ProductImageId)
                .FirstOrDefault();

            if (updatedProductImage != null)
            {
                updatedProductImage = productImage;
            }
        }

        public void DeleteProductImage(ProductImage productImage)
        {
            _productImages.Remove(productImage);
        }

        public ProductImage GetProductImage(int id)
        {
            return _productImages
                .Where(productImage => productImage.ProductImageId == id)
                .FirstOrDefault();
        }

        public ProductImage GetLastProductImage()
        {
            return _productImages
                .OrderBy(productImage => productImage.ProductImageId)
                .LastOrDefault();
        }

        public List<ProductImage> GetProductImages(int productId)
        {
            return _productImages
                .Where(productImage => productImage.ProductId == productId)
                .ToList();
        }
    }
}