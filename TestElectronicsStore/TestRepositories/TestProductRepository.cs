﻿using System.Collections.Generic;
using System.Linq;
using ElectronicsStore.Enums;
using ElectronicsStore.Models;
using ElectronicsStore.Repositories;

namespace TestElectronicsStore.TestRepositories
{
    public class TestProductRepository : IProductRepository
    {
        private List<Product> _products;

        public TestProductRepository()
        {
            _products = new List<Product>()
            {
                new Product()
                {
                    ProductId = 1,
                    Code = "TESTOWY/KOD/1",
                    Name = "Testowy produkt 1",
                    Quantity = 1,
                    Price = 1,
                    OldPrice = 11,
                    Description = "Testowy opis",
                    Information = "Testowe informcje",
                    Characteristics = "Testowe cechy",
                    IsMostPopular = true,
                    SpecialOffer = ElectronicsStore.Enums.SpecialOffer.New,
                    CategoryId = 1,
                    Category = new Category(),
                    Images = null
                },
                new Product()
                {
                    ProductId = 2,
                    Code = "TESTOWY/KOD/2",
                    Name = "Testowy produkt 2",
                    Quantity = 2,
                    Price = 2,
                    OldPrice = 22,
                    Description = "Testowy opis",
                    Information = "Testowe informcje",
                    Characteristics = "Testowe cechy",
                    IsMostPopular = true,
                    SpecialOffer = ElectronicsStore.Enums.SpecialOffer.New,
                    CategoryId = 2,
                    Category = new Category(),
                    Images = null
                },
            };
        }

        public void AddProduct(Product product)
        {
            _products.Add(product);
        }

        public void UpdateProduct(Product product)
        {
            Product updatedProduct = _products
                .Where(product => product.ProductId == product.ProductId)
                .FirstOrDefault();

            if (updatedProduct != null)
            {
                updatedProduct = product;
            }
        }

        public void DeleteProduct(Product product)
        {
            _products.Remove(product);
        }

        public Product GetProduct(int id)
        {
            return  _products
                .Where(product => product.ProductId == id)
                .FirstOrDefault();
        }

        public Product GetLatestProduct()
        {
            return _products
                .OrderBy(product => product.ProductId)
                .LastOrDefault();
        }

        public List<Product> GetProducts()
        {
            return _products
                .ToList();
        }

        public List<Product> GetMostPopularProducts()
        {
            return _products
                .Where(product => product.IsMostPopular == true)
                .ToList();
        }

        public List<Product> GetOutletProducts()
        {
            return _products
                .Where(product => product.SpecialOffer == SpecialOffer.Outlet)
                .ToList();
        }

        public List<Product> GetProductsOnPromotion()
        {
            return _products
                .Where(product => product.SpecialOffer == SpecialOffer.Promotion)
                .ToList();
        }

        public List<Product> GetNewProducts()
        {
            return _products
                .Where(product => product.SpecialOffer == SpecialOffer.New)
                .ToList();
        }

        public List<Product> GetProductsFromCategory(int? id)
        {
            return _products
                .Where(product => product.CategoryId == id)
                .ToList();
        }

        public List<Product> GetSimilarProducts(Product currentProduct)
        {
            return _products
                .Where(product => product.CategoryId == currentProduct.CategoryId && product.ProductId != currentProduct.ProductId)
                .ToList();
        }

        public List<Product> GetLatestProducts(int quantity)
        {
            return _products
                .OrderByDescending(product => product.ProductId)
                .Take(quantity)
                .ToList();
        }
    }
}