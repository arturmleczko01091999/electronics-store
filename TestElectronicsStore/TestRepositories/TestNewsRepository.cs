﻿using System;
using System.Collections.Generic;
using System.Linq;
using ElectronicsStore.Models;
using ElectronicsStore.Repositories;

namespace TestElectronicsStore.TestRepositories
{
    public class TestNewsRepository: INewsRepository
    {
        private List<News> _news;

        public TestNewsRepository()
        {
            _news = new List<News>()
            {
                new News()
                {
                    NewsId = 1,
                    Title = "Testowy tytuł 1",
                    Content = "Testowa treść 1",
                    ImageUrl = "/images/testowa_grafka_1.jpg",
                    ImageTitle = "Testowy tekst alternatywny grafiki 1",
                    Date = new DateTime(2022, 1, 1),
                    isRecommended = true
                },
                new News()
                {
                    NewsId = 2,
                    Title = "Testowy tytuł 2",
                    Content = "Testowa treść 2",
                    ImageUrl = "/images/testowa_grafka_2.jpg",
                    ImageTitle = "Testowy tekst alternatywny grafiki 2",
                    Date = new DateTime(2022, 1, 2),
                    isRecommended = false
                }
            };
        }

        public void AddNews(News news)
        {
            _news.Add(news);
        }

        public void UpdateNews(News news)
        {
            News updatedNews = _news
                .Where(news => news.NewsId == news.NewsId)
                .FirstOrDefault();

            if (updatedNews != null)
            {
                updatedNews = news;
            }
        }

        public void DeleteNews(News news)
        {
            _news.Remove(news);
        }

        public News GetNews(int id)
        {
            return _news
                .Where(news => news.NewsId == id)
                .FirstOrDefault();
        }

        public News GetLastNews()
        {
            return _news
                .OrderBy(news => news.NewsId)
                .LastOrDefault();
        }

        public List<News> GetAllNews()
        {
            return _news
                .OrderByDescending(news => news.Date)
                .ToList();
        }

        public List<News> GetLastThreeNews()
        {
            return _news
                .OrderByDescending(news => news.Date)
                .Take(3)
                .ToList();
        }

        public List<News> GetRecommendedNews()
        {
            return _news
                .Where(news => news.isRecommended == true)
                .OrderByDescending(news => news.Date)
                .ToList();
        }
    }
}
