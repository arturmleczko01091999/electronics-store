﻿using System.Collections.Generic;
using System.Linq;
using ElectronicsStore.Controllers;
using ElectronicsStore.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Newtonsoft.Json;
using TestElectronicsStore.TestRepositories;
using Xunit;

namespace TestElectronicsStore.TestControllers
{
    public class RESTProductPhotoControllerTest
    {
        [Fact]
        public void Update_InvalidProductImageId_ReturnsNotFoundResult()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestProductImageRepository testProductImageRepository = new TestProductImageRepository();

            RESTProductPhotoController controller = new RESTProductPhotoController(testProductImageRepository, mockHostingEnvironment.Object);

            int invalidProductImageId = 10;

            ProductImage testProductImage = new ProductImage()
            {
                ProductImageId = 1,
                Title = "Testowy tytuł 1",
                Url = "/images/testowa_grafka_1.jpg",
                IsMain = true,
                ProductId = 1,
                Product = new Product()
            };

            IActionResult result = controller.UpdateProductPhoto(invalidProductImageId, testProductImage);

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void Update_ValidProductImageId_OkObjectResult()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestProductImageRepository testProductImageRepository = new TestProductImageRepository();

            RESTProductPhotoController controller = new RESTProductPhotoController(testProductImageRepository, mockHostingEnvironment.Object);

            int validProductImageId = 1;

            ProductImage testProductImage = new ProductImage()
            {
                ProductImageId = 1,
                Title = "Testowy tytuł 1",
                Url = "/images/testowa_grafka_1.jpg",
                IsMain = true,
                ProductId = 1,
                Product = new Product()
            };

            IActionResult result = controller.UpdateProductPhoto(validProductImageId, testProductImage);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Update_ProductImageSwapped_ReturnAmendedProductImageCategory()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestProductImageRepository testProductImageRepository = new TestProductImageRepository();

            RESTProductPhotoController controller = new RESTProductPhotoController(testProductImageRepository, mockHostingEnvironment.Object);

            int productImageId = 1;

            ProductImage productImageSwapped = new ProductImage()
            {
                Title = "Testowy tytuł 1",
                Url = "/images/testowa_grafka_1.jpg",
                IsMain = true,
                ProductId = 1,
                Product = new Product()
            };

            OkObjectResult result = (OkObjectResult) controller.UpdateProductPhoto(productImageId, productImageSwapped);
            ProductImage amendedProductImage = (ProductImage) result.Value;

            string amendedProductImageProduct = JsonConvert.SerializeObject(productImageSwapped.Product);
            string productImageSwappedProduct = JsonConvert.SerializeObject(amendedProductImage.Product);

            Assert.Equal(amendedProductImage.Title, productImageSwapped.Title);
            Assert.Equal(amendedProductImage.Url, productImageSwapped.Url);
            Assert.Equal(amendedProductImage.IsMain, productImageSwapped.IsMain);
            Assert.Equal(amendedProductImage.ProductId, productImageSwapped.ProductId);
            Assert.Equal(amendedProductImageProduct, productImageSwappedProduct);
        }

        [Fact]
        public void Delete_InvalidProductImageId_ReturnsNotFoundResult()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestProductImageRepository testProductImageRepository = new TestProductImageRepository();

            RESTProductPhotoController controller = new RESTProductPhotoController(testProductImageRepository, mockHostingEnvironment.Object);

            int invalidProductImageId = 10;

            IActionResult result = controller.DeleteProductPhoto(invalidProductImageId);

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void Delete_ValidProductImageId_ReturnsWhetherThereProductImageWithThisId()
        {
            TestProductImageRepository testProductImageRepository = new TestProductImageRepository();

            int validCategoryId = 1;

            ProductImage productImageToDelete = testProductImageRepository.GetProductImage(validCategoryId);

            Assert.NotNull(productImageToDelete);
        }
    }
}

