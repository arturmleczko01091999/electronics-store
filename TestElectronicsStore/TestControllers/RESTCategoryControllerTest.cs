﻿using System.Collections.Generic;
using System.Linq;
using ElectronicsStore.Controllers;
using ElectronicsStore.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Newtonsoft.Json;
using TestElectronicsStore.TestRepositories;
using Xunit;

namespace TestElectronicsStore.TestControllers
{
    public class RESTCategoryControllerTest
    {
        [Fact]
        public void Add_TestCategory_ReturnsCreatedResult()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestCategoryRepository testCategoryRepository = new TestCategoryRepository();

            RESTCategoryController controller = new RESTCategoryController(testCategoryRepository, mockHostingEnvironment.Object);

            Category testCategory = new Category()
            {
                Name = "Testowa kategoria",
                ImageUrl = "/images/testowa_grafka.jpg",
                ImageTitle = "Testowy tekst alternatywny grafiki",
                Products = new List<Product>()
                {
                    new Product()
                    {
                        ProductId = 1,
                        Name = "Testowy produkt 1"
                    }
                }
            };

            IActionResult result = controller.AddCategory(testCategory);

            Assert.IsType<CreatedResult>(result);
        }

        [Fact]
        public void Add_ActualCategory_ReturnExpectedCategory()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestCategoryRepository testCategoryRepository = new TestCategoryRepository();

            RESTCategoryController controller = new RESTCategoryController(testCategoryRepository, mockHostingEnvironment.Object);

            Category actualCategory = new Category()
            {
                Name = "Testowa kategoria",
                ImageUrl = "/images/testowa_grafka.jpg",
                ImageTitle = "Testowy tekst alternatywny grafiki",
                Products = new List<Product>()
                {
                    new Product()
                    {
                        ProductId = 1,
                        Name = "Testowy produkt 1"
                    }
                }
            };

            CreatedResult result = (CreatedResult) controller.AddCategory(actualCategory);
            Category expectedCategory = (Category) result.Value;

            Assert.Equal(expectedCategory.Name, actualCategory.Name);
            Assert.Equal(expectedCategory.ImageUrl, actualCategory.ImageUrl);
            Assert.Equal(expectedCategory.ImageTitle, actualCategory.ImageTitle);
            Assert.Equal(expectedCategory.Products, actualCategory.Products);
        }

        [Fact]
        public void Add_TestCategory_ReturnsArticleListContainsAddedTestCategory()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestCategoryRepository testCategoryRepository = new TestCategoryRepository();

            RESTCategoryController controller = new RESTCategoryController(testCategoryRepository, mockHostingEnvironment.Object);

            Category testCategory = new Category()
            {
                Name = "Testowa kategoria",
                ImageUrl = "/images/testowa_grafka.jpg",
                ImageTitle = "Testowy tekst alternatywny grafiki",
                Products = new List<Product>()
                {
                    new Product()
                    {
                        ProductId = 1,
                        Name = "Testowy produkt 1"
                    }
                }
            };

            controller.AddCategory(testCategory);

            List<Category> allCategories = testCategoryRepository.GetAllCategories();
            bool isIncludeTestCategory = allCategories.Any(category => category.CategoryId == testCategory.CategoryId);

            Assert.True(isIncludeTestCategory);
        }

        [Fact]
        public void Add_TestCategory_ReturnCategoryListNumberIncrementedByOne()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestCategoryRepository testCategoryRepository = new TestCategoryRepository();

            RESTCategoryController controller = new RESTCategoryController(testCategoryRepository, mockHostingEnvironment.Object);

            Category testCategory = new Category()
            {
                Name = "Testowa kategoria",
                ImageUrl = "/images/testowa_grafka.jpg",
                ImageTitle = "Testowy tekst alternatywny grafiki",
                Products = new List<Product>()
                {
                    new Product()
                    {
                        ProductId = 1,
                        Name = "Testowy produkt 1"
                    }
                }
            };

            int numberCategoriesBeforeAdding = testCategoryRepository.GetAllCategories().Count();

            controller.AddCategory(testCategory);

            int numberCategoriesAfterAdding = testCategoryRepository.GetAllCategories().Count();

            Assert.Equal(++numberCategoriesBeforeAdding, numberCategoriesAfterAdding);
        }

        [Fact]
        public void Update_InvalidCategoryId_ReturnsNotFoundResult()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestCategoryRepository testCategoryRepository = new TestCategoryRepository();

            RESTCategoryController controller = new RESTCategoryController(testCategoryRepository, mockHostingEnvironment.Object);

            int invalidCategoryId = 10;

            Category testCategory = new Category()
            {
                Name = "Testowa kategoria",
                ImageUrl = "/images/testowa_grafka_1.jpg",
                ImageTitle = "Testowy tekst alternatywny grafiki 1",
                Products = new List<Product>()
                {
                    new Product()
                    {
                        ProductId = 1,
                        Name = "Testowy produkt 1"
                    }
                }
            };

            IActionResult result = controller.UpdateCategory(invalidCategoryId, testCategory);

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void Update_ValidCategoryId_OkObjectResult()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestCategoryRepository testCategoryRepository = new TestCategoryRepository();

            RESTCategoryController controller = new RESTCategoryController(testCategoryRepository, mockHostingEnvironment.Object);

            int validCategoryId = 1;

            Category testCategory = new Category()
            {
                Name = "Testowa kategoria",
                ImageUrl = "/images/testowa_grafka_1.jpg",
                ImageTitle = "Testowy tekst alternatywny grafiki 1",
                Products = new List<Product>()
                {
                    new Product()
                    {
                        ProductId = 1,
                        Name = "Testowy produkt 1"
                    }
                }
            };

            IActionResult result = controller.UpdateCategory(validCategoryId, testCategory);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Update_CategorySwapped_ReturnAmendedCategory()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestCategoryRepository testCategoryRepository = new TestCategoryRepository();

            RESTCategoryController controller = new RESTCategoryController(testCategoryRepository, mockHostingEnvironment.Object);

            int categoryId = 1;

            Category categorySwapped = new Category()
            {
                Name = "Testowa kategoria",
                ImageUrl = "/images/testowa_grafka_1.jpg",
                ImageTitle = "Testowy tekst alternatywny grafiki 1",
                Products = new List<Product>()
                {
                    new Product()
                    {
                        ProductId = 1,
                        Name = "Testowy produkt 1"
                    }
                }
            };

            OkObjectResult result = (OkObjectResult) controller.UpdateCategory(categoryId, categorySwapped);
            Category amendedCategory = (Category) result.Value;

            string amendedCategoryProduct = JsonConvert.SerializeObject(amendedCategory.Products);
            string categorySwappedProduct = JsonConvert.SerializeObject(categorySwapped.Products);

            Assert.Equal(amendedCategory.Name, categorySwapped.Name);
            Assert.Equal(amendedCategory.ImageUrl, categorySwapped.ImageUrl);
            Assert.Equal(amendedCategory.ImageTitle, categorySwapped.ImageTitle);
            Assert.Equal(amendedCategoryProduct, categorySwappedProduct);
        }

        [Fact]
        public void Delete_InvalidCategoryId_ReturnsNotFoundResult()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestCategoryRepository testCategoryRepository = new TestCategoryRepository();

            RESTCategoryController controller = new RESTCategoryController(testCategoryRepository, mockHostingEnvironment.Object);

            int invalidCategoryId = 10;

            IActionResult result = controller.DeleteCategory(invalidCategoryId);

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void Delete_ValidArticleId_ReturnsWhetherThereArticleWithThisId()
        {
            TestCategoryRepository testCategoryRepository = new TestCategoryRepository();

            int validCategoryId = 1;

            List<Category> allCategories = testCategoryRepository.GetAllCategories();

            bool isIncludeCategoryToDelete = allCategories.Any(category => category.CategoryId == validCategoryId);

            Assert.True(isIncludeCategoryToDelete);
        }
    }
}

