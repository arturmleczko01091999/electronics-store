﻿using System;
using System.Collections.Generic;
using System.Linq;
using ElectronicsStore.Controllers;
using ElectronicsStore.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Moq;
using TestElectronicsStore.TestRepositories;
using Xunit;

namespace TestElectronicsStore.TestControllers
{
    public class RESTProductControllerTest
    {
        [Fact]
        public void Delete_InvalidProductId_ReturnsNotFoundResult()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestProductRepository testProductRepository = new TestProductRepository();

            RESTProductController controller = new RESTProductController(testProductRepository, mockHostingEnvironment.Object);

            int invalidProductId = 10;

            IActionResult result = controller.DeleteProduct(invalidProductId);

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void Delete_ValidProductId_ReturnsWhetherThereProductWithThisId()
        {
            TestProductRepository testProductRepository = new TestProductRepository();

            int validProductId = 1;

            List<Product> allProducts = testProductRepository.GetProducts();

            bool isIncludeProductToDelete = allProducts.Any(product => product.ProductId == validProductId);

            Assert.True(isIncludeProductToDelete);
        }
    }
}
