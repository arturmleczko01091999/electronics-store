﻿using System;
using System.Collections.Generic;
using System.Linq;
using ElectronicsStore.Controllers;
using ElectronicsStore.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Moq;
using TestElectronicsStore.TestRepositories;
using Xunit;

namespace TestElectronicsStore.TestControllers
{
    public class RESTArticleControllerTest
    {
        [Fact]
        public void Add_TestArticle_ReturnsCreatedResult()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestNewsRepository testNewsRepository = new TestNewsRepository();

            RESTArticleController controller = new RESTArticleController(testNewsRepository, mockHostingEnvironment.Object);

            News testArticle = new News()
            {
                Title = "Testowy tytuł",
                Content = "Testowa treść",
                ImageUrl = "/images/testowa_grafka.jpg",
                ImageTitle = "Testowy tekst alternatywny grafiki",
                Date = new DateTime(2022, 1, 19),
                isRecommended = true
            };

            IActionResult result = controller.AddArticle(testArticle);

            Assert.IsType<CreatedResult>(result);
        }

        [Fact]
        public void Add_ActualArticle_ReturnExpectedArticle()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestNewsRepository testNewsRepository = new TestNewsRepository();

            RESTArticleController controller = new RESTArticleController(testNewsRepository, mockHostingEnvironment.Object);

            News actualArticle = new News()
            {
                Title = "Testowy tytuł",
                Content = "Testowa treść",
                ImageUrl = "/images/testowa_grafka.jpg",
                ImageTitle = "Testowy tekst alternatywny grafiki",
                Date = new DateTime(2022, 1, 19),
                isRecommended = true
            };

            CreatedResult result = (CreatedResult) controller.AddArticle(actualArticle);
            News expectedArticle = (News) result.Value;

            Assert.Equal(expectedArticle.Title, actualArticle.Title);
            Assert.Equal(expectedArticle.Content, actualArticle.Content);
            Assert.Equal(expectedArticle.ImageUrl, actualArticle.ImageUrl);
            Assert.Equal(expectedArticle.ImageTitle, actualArticle.ImageTitle);
            Assert.Equal(expectedArticle.Date, actualArticle.Date);
            Assert.Equal(expectedArticle.isRecommended, actualArticle.isRecommended);
        }

        [Fact]
        public void Add_TestArticle_ReturnsArticleListContainsAddedTestArticle()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestNewsRepository testNewsRepository = new TestNewsRepository();

            RESTArticleController controller = new RESTArticleController(testNewsRepository, mockHostingEnvironment.Object);

            News testArticle = new News()
            {
                Title = "Testowy tytuł",
                Content = "Testowa treść",
                ImageUrl = "/images/testowa_grafka.jpg",
                ImageTitle = "Testowy tekst alternatywny grafiki",
                Date = new DateTime(2022, 1, 19),
                isRecommended = true
            };

            controller.AddArticle(testArticle);

            List<News> allArticles = testNewsRepository.GetAllNews();
            bool isIncludeTestArticle = allArticles.Any(article => article.NewsId == testArticle.NewsId);

            Assert.True(isIncludeTestArticle);
        }

        [Fact]
        public void Add_TestArticle_ReturnArticleListNumberIncrementedByOne()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestNewsRepository testNewsRepository = new TestNewsRepository();

            RESTArticleController controller = new RESTArticleController(testNewsRepository, mockHostingEnvironment.Object);

            News testArticle = new News()
            {
                Title = "Testowy tytuł",
                Content = "Testowa treść",
                ImageUrl = "/images/testowa_grafka.jpg",
                ImageTitle = "Testowy tekst alternatywny grafiki",
                Date = new DateTime(2022, 1, 19),
                isRecommended = true
            };

            int numberArticlesBeforeAdding = testNewsRepository.GetAllNews().Count();

            controller.AddArticle(testArticle);

            int numberArticlesAfterAdding = testNewsRepository.GetAllNews().Count();

            Assert.Equal(++numberArticlesBeforeAdding, numberArticlesAfterAdding);
        }

        [Fact]
        public void Update_InvalidArticleId_ReturnsNotFoundResult()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestNewsRepository testNewsRepository = new TestNewsRepository();

            RESTArticleController controller = new RESTArticleController(testNewsRepository, mockHostingEnvironment.Object);

            int invalidArticleId = 10;

            News testArticle = new News()
            {
                Title = "Zmieniony testowy tytuł",
                Content = "Zmieniona testowa treść",
                ImageUrl = "/images/testowa_grafka_1.jpg",
                ImageTitle = "Testowy tekst alternatywny grafiki 1",
                Date = new DateTime(2022, 1, 18),
                isRecommended = false
            };

            IActionResult result = controller.UpdateArticle(invalidArticleId, testArticle);

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void Update_ValidArticleId_OkObjectResult()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestNewsRepository testNewsRepository = new TestNewsRepository();

            RESTArticleController controller = new RESTArticleController(testNewsRepository, mockHostingEnvironment.Object);

            int validArticleId = 1;

            News testArticle = new News()
            {
                Title = "Zmieniony testowy tytuł",
                Content = "Zmieniona testowa treść",
                ImageUrl = "/images/testowa_grafka_1.jpg",
                ImageTitle = "Testowy tekst alternatywny grafiki 1",
                Date = new DateTime(2022, 1, 18),
                isRecommended = false
            };

            IActionResult result = controller.UpdateArticle(validArticleId, testArticle);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Update_ArticleSwapped_ReturnAmendedArticle()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestNewsRepository testNewsRepository = new TestNewsRepository();

            RESTArticleController controller = new RESTArticleController(testNewsRepository, mockHostingEnvironment.Object);

            int articleId = 1;

            News articleSwapped = new News()
            {
                Title = "Zmieniony testowy tytuł",
                Content = "Zmieniona testowa treść",
                ImageUrl = "/images/testowa_grafka_1.jpg",
                ImageTitle = "Testowy tekst alternatywny grafiki 1",
                Date = new DateTime(2022, 1, 18),
                isRecommended = false
            };

            OkObjectResult result = (OkObjectResult) controller.UpdateArticle(articleId, articleSwapped);
            News amendedArticle = (News)result.Value;

            Assert.Equal(amendedArticle.Title, articleSwapped.Title);
            Assert.Equal(amendedArticle.Content, articleSwapped.Content);
            Assert.Equal(amendedArticle.ImageUrl, articleSwapped.ImageUrl);
            Assert.Equal(amendedArticle.ImageTitle, articleSwapped.ImageTitle);
            Assert.Equal(amendedArticle.Date, articleSwapped.Date);
            Assert.Equal(amendedArticle.isRecommended, articleSwapped.isRecommended);
        }

        [Fact]
        public void UpdateProperty_InvalidArticleId_ReturnsNotFoundResult()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestNewsRepository testNewsRepository = new TestNewsRepository();

            RESTArticleController controller = new RESTArticleController(testNewsRepository, mockHostingEnvironment.Object);

            int invalidArticleId = 10;

            News articleWithChangedProperties = new News()
            {
                isRecommended = false
            };

            IActionResult result = controller.UpdateArticleContent(invalidArticleId, articleWithChangedProperties);

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void UpdateProperty_ValidArticleId_OkObjectResult()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestNewsRepository testNewsRepository = new TestNewsRepository();

            RESTArticleController controller = new RESTArticleController(testNewsRepository, mockHostingEnvironment.Object);

            int validArticleId = 1;

            News articleWithChangedProperties = new News()
            {
                ImageUrl = "/images/testowa_grafka_1.jpg",
                isRecommended = false
            };

            IActionResult result = controller.UpdateArticleContent(validArticleId, articleWithChangedProperties);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void UpdateProperty_ArticleWithChangedProperties_ReturnExpectedArticleWithChangedProperties()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestNewsRepository testNewsRepository = new TestNewsRepository();

            RESTArticleController controller = new RESTArticleController(testNewsRepository, mockHostingEnvironment.Object);

            int articleId = 1;

            News articleWithChangedProperties = new News()
            {
                ImageUrl = "/images/testowa_grafka_1.jpg",
                isRecommended = false
            };

            OkObjectResult result = (OkObjectResult) controller.UpdateArticle(articleId, articleWithChangedProperties);
            News expectedArticleWithChangedProperty = (News)result.Value;

            Assert.Equal(expectedArticleWithChangedProperty.isRecommended, articleWithChangedProperties.isRecommended);
        }

        [Fact]
        public void Delete_InvalidArticleId_ReturnsNotFoundResult()
        {
            Mock<IWebHostEnvironment> mockHostingEnvironment = new Mock<IWebHostEnvironment>();
            TestNewsRepository testNewsRepository = new TestNewsRepository();

            RESTArticleController controller = new RESTArticleController(testNewsRepository, mockHostingEnvironment.Object);

            int invalidArticleId = 10;

            IActionResult result = controller.DeleteArticle(invalidArticleId);

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void Delete_ValidArticleId_ReturnsWhetherThereArticleWithThisId()
        {
            TestNewsRepository testNewsRepository = new TestNewsRepository();

            int validArticleId = 1;

            List<News> allArticles = testNewsRepository.GetAllNews();

            bool isIncludeArticleToDelete = allArticles.Any(article => article.NewsId == validArticleId);

            Assert.True(isIncludeArticleToDelete);
        }
    }
}
