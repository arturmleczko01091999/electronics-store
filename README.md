
# ElectonicsStore

ElectronicsStore jest to platforma, która pozwala w łatwy sposób stworzyć sklep internetowy oraz nim zarządzać.


## Instalacja

W pierwszym kroku należy sklonować repozytorium.

```bash
  git clone https://arturmleczko01091999@bitbucket.org/arturmleczko01091999/
  electronics-store.git
```

W celu uruchomienia aplikacji wymagane jest zaopatrzenie się w system do zarządzania relacyjnymi bazami danych — PostgreSQL.

```bash
  https://www.postgresql.org/download/
  https://www.pgadmin.org/download/
```  

Po zainstalowaniu oprogramowania należy utworzyć bazę danych o nazwie **electronics_store**, aby połączyć aplikację ASP.NET z systemem bazodanowym.

```bash
  "ConnectionStrings": {
    "DefaultConnection": "Server=localhost;database=electronics_store"
  }
```  
![App Screenshot](https://i.ibb.co/GpMGDs1/Zrzut-ekranu-2022-01-21-o-22-16-43.png)

Ostatnią rzeczą do uruchomienia systemu jest przeprowadzenia migracji w celu utworzenia struktur bazodanowych na postawie klas modeli i kontekstu.

Wykonujemy poniższe polecenia w konsoli NuGet

```bash
  add-migration InitialCreate
  update-database -verbose

```  

Lub CLI

```bash
  dotnet ef migrations add InitialCreate
  dotnet ef database update

``` 
## Niedokończone funkcjonalności

Elementy widoku, które nie mają stworzonej mechaniki.

- Wyszukiwarka produktów  
![App Screenshot](https://i.ibb.co/VT9Ssq4/Zrzut-ekranu-2022-01-22-o-11-01-09.png)
- Checkbox "Zapamiętaj mnie" i odnośnik "Nie pamiętam hasła"  
![App Screenshot](https://i.ibb.co/G0fcYTV/Zrzut-ekranu-2022-01-22-o-10-59-46.png)
- Koszyk  
![App Screenshot](https://i.ibb.co/bJJpYCp/Zrzut-ekranu-2022-01-22-o-11-01-36.png)
- Dodawanie produktu do koszyka  
![App Screenshot](https://i.ibb.co/h9WTwn9/Zrzut-ekranu-2022-01-22-o-11-05-38.png)

